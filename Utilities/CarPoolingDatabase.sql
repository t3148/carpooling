IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Cities] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Cities] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Preferences] (
    [Id] int NOT NULL IDENTITY,
    [Text] nvarchar(max) NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Preferences] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Roles] (
    [Id] int NOT NULL IDENTITY,
    [Type] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Statuses] (
    [Id] int NOT NULL IDENTITY,
    [Type] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Statuses] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Users] (
    [Id] int NOT NULL IDENTITY,
    [Username] nvarchar(20) NOT NULL,
    [Password] nvarchar(max) NOT NULL,
    [FirstName] nvarchar(20) NOT NULL,
    [LastName] nvarchar(20) NOT NULL,
    [Email] nvarchar(450) NOT NULL,
    [PhoneNumber] nvarchar(450) NOT NULL,
    [ProfilePicture] nvarchar(max) NULL,
    [IsDeleted] bit NOT NULL,
    [IsBlocked] bit NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Feedbacks] (
    [Id] int NOT NULL IDENTITY,
    [Rating] float NOT NULL,
    [FromUserId] int NOT NULL,
    [ToUserId] int NOT NULL,
    [FromDriverToPassenger] bit NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Feedbacks] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Feedbacks_Users_FromUserId] FOREIGN KEY ([FromUserId]) REFERENCES [Users] ([Id]),
    CONSTRAINT [FK_Feedbacks_Users_ToUserId] FOREIGN KEY ([ToUserId]) REFERENCES [Users] ([Id])
);
GO

CREATE TABLE [Travels] (
    [Id] int NOT NULL IDENTITY,
    [StartingPointId] int NOT NULL,
    [EndingPointId] int NOT NULL,
    [DepartureTime] datetime2 NOT NULL,
    [FreeSeats] int NOT NULL,
    [CreatedByUserId] int NOT NULL,
    [StatusId] int NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Travels] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Travels_Cities_EndingPointId] FOREIGN KEY ([EndingPointId]) REFERENCES [Cities] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Travels_Cities_StartingPointId] FOREIGN KEY ([StartingPointId]) REFERENCES [Cities] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Travels_Statuses_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [Statuses] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Travels_Users_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [Users] ([Id])
);
GO

CREATE TABLE [UserRoles] (
    [Id] int NOT NULL IDENTITY,
    [UserId] int NOT NULL,
    [RoleId] int NOT NULL,
    CONSTRAINT [PK_UserRoles] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UserRoles_Roles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [Roles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserRoles_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE CASCADE
);
GO

CREATE TABLE [Comments] (
    [Id] int NOT NULL IDENTITY,
    [Text] nvarchar(max) NOT NULL,
    [FeedbackId] int NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Comments] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Comments_Feedbacks_FeedbackId] FOREIGN KEY ([FeedbackId]) REFERENCES [Feedbacks] ([Id]) ON DELETE CASCADE
);
GO

CREATE TABLE [ApprovedPassengers] (
    [Id] int NOT NULL IDENTITY,
    [TravelId] int NOT NULL,
    [UserId] int NOT NULL,
    CONSTRAINT [PK_ApprovedPassengers] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ApprovedPassengers_Travels_TravelId] FOREIGN KEY ([TravelId]) REFERENCES [Travels] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ApprovedPassengers_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE CASCADE
);
GO

CREATE TABLE [PendingPassengers] (
    [Id] int NOT NULL IDENTITY,
    [TravelId] int NOT NULL,
    [UserId] int NOT NULL,
    CONSTRAINT [PK_PendingPassengers] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_PendingPassengers_Travels_TravelId] FOREIGN KEY ([TravelId]) REFERENCES [Travels] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_PendingPassengers_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE CASCADE
);
GO

CREATE TABLE [TravelPreferences] (
    [Id] int NOT NULL IDENTITY,
    [TravelId] int NOT NULL,
    [PreferenceId] int NOT NULL,
    CONSTRAINT [PK_TravelPreferences] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_TravelPreferences_Preferences_PreferenceId] FOREIGN KEY ([PreferenceId]) REFERENCES [Preferences] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_TravelPreferences_Travels_TravelId] FOREIGN KEY ([TravelId]) REFERENCES [Travels] ([Id]) ON DELETE CASCADE
);
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Name') AND [object_id] = OBJECT_ID(N'[Cities]'))
    SET IDENTITY_INSERT [Cities] ON;
INSERT INTO [Cities] ([Id], [Name])
VALUES (1, N'Asenovgrad'),
(30, N'Yambol'),
(28, N'Targovishte'),
(27, N'Troyan'),
(26, N'Topolovgrad'),
(25, N'Stara Zagora'),
(24, N'Sofia'),
(23, N'Sliven'),
(22, N'Sevlievo'),
(21, N'Svilengrad'),
(20, N'Ruse'),
(19, N'Razgrad'),
(18, N'Plovdiv'),
(17, N'Pernik'),
(16, N'Pazardzhik'),
(29, N'Haskovo'),
(14, N'Lovech'),
(15, N'Montana'),
(2, N'Bansko'),
(3, N'Blagoevgrad'),
(4, N'Bourgas'),
(5, N'Varna'),
(7, N'Velingrad'),
(6, N'Veliko Tarnovo'),
(9, N'Vraca'),
(10, N'Gabrovo'),
(11, N'Dimitrovgrad'),
(12, N'Etropole'),
(13, N'Karlovo'),
(8, N'Vidin');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Name') AND [object_id] = OBJECT_ID(N'[Cities]'))
    SET IDENTITY_INSERT [Cities] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'IsDeleted', N'Text') AND [object_id] = OBJECT_ID(N'[Preferences]'))
    SET IDENTITY_INSERT [Preferences] ON;
INSERT INTO [Preferences] ([Id], [IsDeleted], [Text])
VALUES (6, CAST(0 AS bit), N'Free music choice'),
(8, CAST(0 AS bit), N'No drinking'),
(5, CAST(0 AS bit), N'Air Conditioning'),
(7, CAST(0 AS bit), N'No political discussions'),
(3, CAST(0 AS bit), N'No sleeping'),
(2, CAST(0 AS bit), N'No luggage'),
(1, CAST(0 AS bit), N'No smoking'),
(4, CAST(0 AS bit), N'Pet-friendly');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'IsDeleted', N'Text') AND [object_id] = OBJECT_ID(N'[Preferences]'))
    SET IDENTITY_INSERT [Preferences] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Type') AND [object_id] = OBJECT_ID(N'[Roles]'))
    SET IDENTITY_INSERT [Roles] ON;
INSERT INTO [Roles] ([Id], [Type])
VALUES (1, N'User'),
(2, N'Admin');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Type') AND [object_id] = OBJECT_ID(N'[Roles]'))
    SET IDENTITY_INSERT [Roles] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Type') AND [object_id] = OBJECT_ID(N'[Statuses]'))
    SET IDENTITY_INSERT [Statuses] ON;
INSERT INTO [Statuses] ([Id], [Type])
VALUES (1, N'Available'),
(2, N'Completed');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Type') AND [object_id] = OBJECT_ID(N'[Statuses]'))
    SET IDENTITY_INSERT [Statuses] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Email', N'FirstName', N'IsBlocked', N'IsDeleted', N'LastName', N'Password', N'PhoneNumber', N'ProfilePicture', N'Username') AND [object_id] = OBJECT_ID(N'[Users]'))
    SET IDENTITY_INSERT [Users] ON;
INSERT INTO [Users] ([Id], [Email], [FirstName], [IsBlocked], [IsDeleted], [LastName], [Password], [PhoneNumber], [ProfilePicture], [Username])
VALUES (13, N'vascoc@gmail.com', N'Vasil', CAST(0 AS bit), CAST(0 AS bit), N'Tsvyatkov', N'TestPass-123', N'0889528888', N'c83fd0bc-9bbb-4dcc-9aa2-d46f8faf8837_face9.jpg', N'djvascoc'),
(12, N'milenа@gmail.com', N'Milenа', CAST(0 AS bit), CAST(0 AS bit), N'Stefanovа', N'TestPass-123', N'0883782104', N'd57065c9-80e5-4c1f-bb02-71ff5f92b6b7_face8.jpg', N'milenа96'),
(11, N'victor@gmail.com', N'Victor', CAST(0 AS bit), CAST(0 AS bit), N'Yordanov', N'TestPass-123', N'0896825109', N'c7f485d1-3f1c-4356-ac54-a7cdeab8b167_face7.jpg', N'viksana'),
(10, N'sstefanov@gmail.com', N'Stefan', CAST(0 AS bit), CAST(0 AS bit), N'Stefanov', N'TestPass-123', N'0896823475', N'4addf827-4e6b-4280-8285-4a5fa4886a79_face6.jpg', N'junior_dev'),
(9, N'simo911@yahoo.com', N'Simo', CAST(0 AS bit), CAST(0 AS bit), N'Ivaylov', N'TestPass-123', N'0896882245', N'15da5fd0-132e-47ac-97a5-6e2772310f04_face5.jpg', N'simeon911'),
(8, N'mikedev@gmail.com', N'Mike', CAST(0 AS bit), CAST(0 AS bit), N'Wazowski', N'TestPass-123', N'0898123456', N'64da4c87-752f-4d07-80ac-42a5e59238b4_face4.jpg', N'mike.dev'),
(4, N'petar.raykov@abv.bg', N'Petar', CAST(0 AS bit), CAST(0 AS bit), N'Raykov', N'TestPass-123', N'0893564789', N'66ac03d5-6c4c-42cb-b5cf-189cb20423bd_praykov.jpg', N'p_raykov'),
(6, N'marinova123@consulting.net', N'Mariya', CAST(0 AS bit), CAST(0 AS bit), N'Marinova', N'TestPass-123', N'0897745093', N'6877f653-0c54-4f1b-829e-6eb44ecaef57_face2.jpg', N'mmarinova'),
(5, N'i_petrov@mail.bg', N'Ivan', CAST(0 AS bit), CAST(0 AS bit), N'Petrov', N'TestPass-123', N'0888930900', N'f33ec6a7-7a02-4e75-8cd3-c6445d47de06_face1.jpg', N'petrovv'),
(3, N'kiril.stanoev@yahoo.com', N'Kiril', CAST(0 AS bit), CAST(0 AS bit), N'Stanoev', N'TestPass-123', N'0899353878', N'cef26811-1f8e-4b8c-8cad-54d0e1622a0c_kstanoev.jpg', N'k.stanoev'),
(2, N'alex.taskovv@gmail.com', N'Alexander', CAST(0 AS bit), CAST(0 AS bit), N'Taskov', N'TestPass-123', N'0898322922', N'7b01f2ba-454d-40d4-b6be-8c4e2cf883c7_sahezarabota.jpg', N'ataskov'),
(1, N'bogdan.kehayov@gmail.com', N'Bogdan', CAST(0 AS bit), CAST(0 AS bit), N'Kehayov', N'TestPass-123', N'0894747949', N'c504be68-ba0f-4a72-8314-b0b0c15df4cc_82647092_10213410429654656_7370158159039561728_n.jpg', N'bkehayov'),
(14, N'kkalinov@abv.bg', N'Kalin', CAST(0 AS bit), CAST(0 AS bit), N'Kalinov', N'TestPass-123', N'0887410410', N'd611aed2-6a0b-4d30-8bbd-7b7afd901b9b_face10.jpg', N'ka_lin'),
(7, N'georgi@mail.bg', N'Georgi', CAST(0 AS bit), CAST(0 AS bit), N'Zaykov', N'TestPass-123', N'0898921812', N'af700f7c-8b22-4165-8b9b-9ba01b7de249_face3.jpg', N'zayykov'),
(15, N'dzhamulov@soft.com', N'Ivan', CAST(0 AS bit), CAST(0 AS bit), N'Dzhamulov', N'TestPass-123', N'0887555963', N'6e9f08d6-dcb4-4b20-b100-773b0a8b29fd_face11.jpg', N'dzhamulov');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Email', N'FirstName', N'IsBlocked', N'IsDeleted', N'LastName', N'Password', N'PhoneNumber', N'ProfilePicture', N'Username') AND [object_id] = OBJECT_ID(N'[Users]'))
    SET IDENTITY_INSERT [Users] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FromDriverToPassenger', N'FromUserId', N'IsDeleted', N'Rating', N'ToUserId') AND [object_id] = OBJECT_ID(N'[Feedbacks]'))
    SET IDENTITY_INSERT [Feedbacks] ON;
INSERT INTO [Feedbacks] ([Id], [FromDriverToPassenger], [FromUserId], [IsDeleted], [Rating], [ToUserId])
VALUES (30, CAST(1 AS bit), 6, CAST(0 AS bit), 3.8999999999999999E0, 8),
(1, CAST(1 AS bit), 3, CAST(0 AS bit), 3.8999999999999999E0, 4),
(2, CAST(0 AS bit), 3, CAST(0 AS bit), 2.0E0, 4),
(8, CAST(1 AS bit), 9, CAST(0 AS bit), 3.0E0, 10),
(33, CAST(0 AS bit), 9, CAST(0 AS bit), 5.0E0, 4),
(3, CAST(1 AS bit), 3, CAST(0 AS bit), 3.0E0, 5),
(4, CAST(0 AS bit), 5, CAST(0 AS bit), 4.0E0, 4),
(31, CAST(0 AS bit), 9, CAST(0 AS bit), 4.2999999999999998E0, 3),
(32, CAST(1 AS bit), 6, CAST(0 AS bit), 5.0E0, 4),
(34, CAST(1 AS bit), 6, CAST(0 AS bit), 5.0E0, 3),
(29, CAST(0 AS bit), 9, CAST(0 AS bit), 4.0E0, 6),
(5, CAST(1 AS bit), 6, CAST(0 AS bit), 5.0E0, 7),
(35, CAST(0 AS bit), 7, CAST(0 AS bit), 5.0E0, 1),
(39, CAST(0 AS bit), 7, CAST(0 AS bit), 4.2999999999999998E0, 7),
(7, CAST(0 AS bit), 8, CAST(0 AS bit), 5.0E0, 9),
(6, CAST(1 AS bit), 8, CAST(0 AS bit), 2.0E0, 9),
(28, CAST(1 AS bit), 3, CAST(0 AS bit), 3.0E0, 8),
(23, CAST(1 AS bit), 15, CAST(0 AS bit), 3.0E0, 2),
(9, CAST(0 AS bit), 9, CAST(0 AS bit), 4.0E0, 10),
(10, CAST(1 AS bit), 10, CAST(0 AS bit), 4.0E0, 11),
(11, CAST(0 AS bit), 10, CAST(0 AS bit), 5.0E0, 11),
(38, CAST(1 AS bit), 11, CAST(0 AS bit), 3.7999999999999998E0, 5),
(22, CAST(0 AS bit), 15, CAST(0 AS bit), 4.2999999999999998E0, 2),
(21, CAST(0 AS bit), 15, CAST(0 AS bit), 2.0E0, 1),
(20, CAST(1 AS bit), 15, CAST(0 AS bit), 1.0E0, 1),
(19, CAST(0 AS bit), 14, CAST(0 AS bit), 3.0E0, 15),
(18, CAST(1 AS bit), 14, CAST(0 AS bit), 5.0E0, 15),
(40, CAST(1 AS bit), 14, CAST(0 AS bit), 3.8999999999999999E0, 7),
(36, CAST(1 AS bit), 14, CAST(0 AS bit), 4.5E0, 1),
(17, CAST(0 AS bit), 13, CAST(0 AS bit), 3.0E0, 14),
(41, CAST(0 AS bit), 6, CAST(0 AS bit), 4.2999999999999998E0, 8),
(16, CAST(1 AS bit), 13, CAST(0 AS bit), 4.0E0, 14),
(25, CAST(1 AS bit), 13, CAST(0 AS bit), 5.0E0, 5),
(24, CAST(0 AS bit), 13, CAST(0 AS bit), 4.0E0, 5),
(15, CAST(1 AS bit), 12, CAST(0 AS bit), 3.2000000000000002E0, 13),
(14, CAST(0 AS bit), 12, CAST(0 AS bit), 4.7000000000000002E0, 13),
(27, CAST(0 AS bit), 12, CAST(0 AS bit), 2.0E0, 9),
(26, CAST(1 AS bit), 12, CAST(0 AS bit), 1.0E0, 9),
(13, CAST(1 AS bit), 11, CAST(0 AS bit), 4.0E0, 12),
(12, CAST(0 AS bit), 11, CAST(0 AS bit), 5.0E0, 12),
(37, CAST(0 AS bit), 13, CAST(0 AS bit), 4.5999999999999996E0, 5),
(42, CAST(1 AS bit), 3, CAST(0 AS bit), 2.7999999999999998E0, 8);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FromDriverToPassenger', N'FromUserId', N'IsDeleted', N'Rating', N'ToUserId') AND [object_id] = OBJECT_ID(N'[Feedbacks]'))
    SET IDENTITY_INSERT [Feedbacks] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedByUserId', N'DepartureTime', N'EndingPointId', N'FreeSeats', N'IsDeleted', N'StartingPointId', N'StatusId') AND [object_id] = OBJECT_ID(N'[Travels]'))
    SET IDENTITY_INSERT [Travels] ON;
INSERT INTO [Travels] ([Id], [CreatedByUserId], [DepartureTime], [EndingPointId], [FreeSeats], [IsDeleted], [StartingPointId], [StatusId])
VALUES (4, 1, '2021-12-18T13:30:00.0000000', 6, 4, CAST(0 AS bit), 5, 1),
(11, 3, '2022-10-18T05:50:00.0000000', 14, 6, CAST(0 AS bit), 27, 1),
(16, 2, '2021-03-17T22:00:00.0000000', 17, 4, CAST(0 AS bit), 13, 2),
(17, 2, '2021-07-19T15:50:00.0000000', 3, 6, CAST(0 AS bit), 5, 2),
(20, 2, '2022-06-05T08:50:00.0000000', 3, 3, CAST(0 AS bit), 27, 1),
(9, 2, '2021-12-11T21:10:00.0000000', 6, 3, CAST(0 AS bit), 23, 1),
(8, 2, '2022-01-12T11:25:00.0000000', 19, 2, CAST(0 AS bit), 12, 1),
(6, 2, '2022-02-02T09:45:00.0000000', 13, 5, CAST(0 AS bit), 15, 1),
(1, 3, '2022-03-15T10:30:00.0000000', 2, 3, CAST(0 AS bit), 1, 1),
(2, 3, '2022-01-12T15:30:00.0000000', 10, 3, CAST(0 AS bit), 9, 1),
(13, 2, '2021-03-15T22:00:00.0000000', 13, 4, CAST(0 AS bit), 18, 2),
(10, 2, '2021-12-15T22:00:00.0000000', 20, 4, CAST(0 AS bit), 29, 1),
(18, 3, '2021-06-12T18:50:00.0000000', 17, 3, CAST(0 AS bit), 18, 2),
(3, 4, '2021-12-15T20:30:00.0000000', 4, 2, CAST(0 AS bit), 3, 1),
(19, 1, '2022-07-11T17:50:00.0000000', 23, 2, CAST(0 AS bit), 15, 1),
(15, 1, '2021-08-14T08:50:00.0000000', 7, 3, CAST(0 AS bit), 9, 2),
(14, 1, '2021-07-18T05:50:00.0000000', 30, 6, CAST(0 AS bit), 20, 2),
(5, 1, '2022-03-15T10:30:00.0000000', 2, 3, CAST(0 AS bit), 7, 1),
(7, 7, '2021-12-20T18:15:00.0000000', 18, 5, CAST(0 AS bit), 20, 1),
(12, 3, '2021-12-14T08:50:00.0000000', 26, 3, CAST(0 AS bit), 17, 1);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedByUserId', N'DepartureTime', N'EndingPointId', N'FreeSeats', N'IsDeleted', N'StartingPointId', N'StatusId') AND [object_id] = OBJECT_ID(N'[Travels]'))
    SET IDENTITY_INSERT [Travels] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'RoleId', N'UserId') AND [object_id] = OBJECT_ID(N'[UserRoles]'))
    SET IDENTITY_INSERT [UserRoles] ON;
INSERT INTO [UserRoles] ([Id], [RoleId], [UserId])
VALUES (11, 1, 1),
(16, 1, 14),
(1, 2, 1),
(7, 1, 7),
(14, 1, 12),
(8, 1, 8),
(2, 2, 2),
(12, 1, 2),
(13, 1, 11),
(10, 1, 10),
(3, 1, 3),
(9, 1, 9),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(15, 1, 13),
(17, 1, 15);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'RoleId', N'UserId') AND [object_id] = OBJECT_ID(N'[UserRoles]'))
    SET IDENTITY_INSERT [UserRoles] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'TravelId', N'UserId') AND [object_id] = OBJECT_ID(N'[ApprovedPassengers]'))
    SET IDENTITY_INSERT [ApprovedPassengers] ON;
INSERT INTO [ApprovedPassengers] ([Id], [TravelId], [UserId])
VALUES (3, 4, 2),
(9, 7, 3),
(8, 7, 11),
(7, 7, 7),
(1, 1, 2),
(2, 2, 11),
(6, 6, 3),
(4, 6, 9),
(5, 6, 10);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'TravelId', N'UserId') AND [object_id] = OBJECT_ID(N'[ApprovedPassengers]'))
    SET IDENTITY_INSERT [ApprovedPassengers] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FeedbackId', N'IsDeleted', N'Text') AND [object_id] = OBJECT_ID(N'[Comments]'))
    SET IDENTITY_INSERT [Comments] ON;
INSERT INTO [Comments] ([Id], [FeedbackId], [IsDeleted], [Text])
VALUES (1, 1, CAST(0 AS bit), N'Great passenger'),
(2, 4, CAST(0 AS bit), N'Bumpy ride');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FeedbackId', N'IsDeleted', N'Text') AND [object_id] = OBJECT_ID(N'[Comments]'))
    SET IDENTITY_INSERT [Comments] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'TravelId', N'UserId') AND [object_id] = OBJECT_ID(N'[PendingPassengers]'))
    SET IDENTITY_INSERT [PendingPassengers] ON;
INSERT INTO [PendingPassengers] ([Id], [TravelId], [UserId])
VALUES (9, 7, 15),
(8, 7, 9),
(7, 7, 1),
(1, 5, 8),
(3, 3, 9),
(2, 3, 3),
(4, 6, 5),
(5, 6, 6),
(6, 6, 7);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'TravelId', N'UserId') AND [object_id] = OBJECT_ID(N'[PendingPassengers]'))
    SET IDENTITY_INSERT [PendingPassengers] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'PreferenceId', N'TravelId') AND [object_id] = OBJECT_ID(N'[TravelPreferences]'))
    SET IDENTITY_INSERT [TravelPreferences] ON;
INSERT INTO [TravelPreferences] ([Id], [PreferenceId], [TravelId])
VALUES (4, 2, 2),
(16, 5, 7),
(13, 2, 6),
(14, 3, 6),
(15, 7, 6),
(1, 1, 1),
(2, 4, 1),
(17, 4, 7),
(3, 5, 1),
(11, 4, 3),
(6, 7, 3),
(5, 3, 3),
(7, 2, 1),
(8, 3, 1),
(9, 6, 1),
(10, 3, 2),
(12, 8, 3),
(18, 3, 7);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'PreferenceId', N'TravelId') AND [object_id] = OBJECT_ID(N'[TravelPreferences]'))
    SET IDENTITY_INSERT [TravelPreferences] OFF;
GO

CREATE INDEX [IX_ApprovedPassengers_TravelId] ON [ApprovedPassengers] ([TravelId]);
GO

CREATE INDEX [IX_ApprovedPassengers_UserId] ON [ApprovedPassengers] ([UserId]);
GO

CREATE UNIQUE INDEX [IX_Comments_FeedbackId] ON [Comments] ([FeedbackId]);
GO

CREATE INDEX [IX_Feedbacks_FromUserId] ON [Feedbacks] ([FromUserId]);
GO

CREATE INDEX [IX_Feedbacks_ToUserId] ON [Feedbacks] ([ToUserId]);
GO

CREATE INDEX [IX_PendingPassengers_TravelId] ON [PendingPassengers] ([TravelId]);
GO

CREATE INDEX [IX_PendingPassengers_UserId] ON [PendingPassengers] ([UserId]);
GO

CREATE INDEX [IX_TravelPreferences_PreferenceId] ON [TravelPreferences] ([PreferenceId]);
GO

CREATE INDEX [IX_TravelPreferences_TravelId] ON [TravelPreferences] ([TravelId]);
GO

CREATE INDEX [IX_Travels_CreatedByUserId] ON [Travels] ([CreatedByUserId]);
GO

CREATE INDEX [IX_Travels_EndingPointId] ON [Travels] ([EndingPointId]);
GO

CREATE INDEX [IX_Travels_StartingPointId] ON [Travels] ([StartingPointId]);
GO

CREATE INDEX [IX_Travels_StatusId] ON [Travels] ([StatusId]);
GO

CREATE INDEX [IX_UserRoles_RoleId] ON [UserRoles] ([RoleId]);
GO

CREATE INDEX [IX_UserRoles_UserId] ON [UserRoles] ([UserId]);
GO

CREATE UNIQUE INDEX [IX_Users_Email] ON [Users] ([Email]);
GO

CREATE UNIQUE INDEX [IX_Users_PhoneNumber] ON [Users] ([PhoneNumber]);
GO

CREATE UNIQUE INDEX [IX_Users_Username] ON [Users] ([Username]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20211209152104_Initial', N'5.0.12');
GO

COMMIT;
GO