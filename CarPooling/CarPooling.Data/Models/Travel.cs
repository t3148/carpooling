﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data.Models
{
    public class Travel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int StartingPointId { get; set; }

        public City StartingPoint { get; set; }

        [Required]
        public int EndingPointId { get; set; }

        public City EndingPoint { get; set; }

        [Required]
        public DateTime DepartureTime { get; set; }

        [Required]
        public int FreeSeats { get; set; }

        [Required]
        public int CreatedByUserId { get; set; }

        public User CreatedByUser { get; set; }

        [Required]
        public int StatusId { get; set; }

        public Status Status { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        public ICollection<PendingPassenger> PendingPassengers { get; set; } = new List<PendingPassenger>();

        public ICollection<ApprovedPassenger> ApprovedPassengers { get; set; } = new List<ApprovedPassenger>();

        public ICollection<TravelPreference> Preferences { get; set; } = new List<TravelPreference>();
    }
}
