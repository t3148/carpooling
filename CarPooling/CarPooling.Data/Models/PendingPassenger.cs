﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data.Models
{
    public class PendingPassenger
    {
        [Key]
        public int Id { get; set; }

        public int TravelId { get; set; }

        public Travel Travel { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }
    }
}
