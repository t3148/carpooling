﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data.Models
{
    public class Feedback
    {
        [Key]
        public int Id { get; set; }

        [Range(0, 5)]
        public double Rating { get; set; }

        [Required]
        public int FromUserId { get; set; }

        public User FromUser { get; set; }

        [Required]
        public int ToUserId { get; set; }

        public User ToUser { get; set; }

        public bool FromDriverToPassenger { get; set; }

        public Comment Comment { get; set; }

        [Required]
        public bool IsDeleted { get; set; }
    }
}
