﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data.Models
{
    public class Preference
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        public ICollection<TravelPreference> Travels { get; set; } = new List<TravelPreference>();

        public bool IsDeleted { get; set; }
    }
}
