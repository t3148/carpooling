﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required, MinLength(2), MaxLength(20)]
        public string Username { get; set; }

        [Required]
        [RegularExpression(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=-_]).*$", ErrorMessage = "Password must contain: One upper case letter; One lower case letter; A number; A special symbol(!*@#$%^&+=-).")]
        public string Password { get; set; }

        [Required, MinLength(2), MaxLength(20)]
        public string FirstName { get; set; }

        [Required, MinLength(2), MaxLength(20)]
        public string LastName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Phone Number!")]
        public string PhoneNumber { get; set; }

        public string ProfilePicture { get; set; }


        [Required]
        public bool IsDeleted { get; set; }

        [Required]
        public bool IsBlocked { get; set; }

        public ICollection<PendingPassenger> PendingTravels { get; set; } = new List<PendingPassenger>();

        public ICollection<ApprovedPassenger> ApprovedTravels { get; set; } = new List<ApprovedPassenger>();

        public ICollection<UserRole> Roles { get; set; } = new List<UserRole>();

        public ICollection<Travel> Travels { get; set; } = new List<Travel>();

        //Feedbacks Fluent API
        public ICollection<Feedback> ReceivedFeedbacks { get; set; } = new List<Feedback>();

        public ICollection<Feedback> GivenFeedbacks { get; set; } = new List<Feedback>();
    }
}
