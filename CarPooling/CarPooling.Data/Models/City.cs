﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data.Models
{
    public class City
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Travel> StartingPoints { get; set; } = new List<Travel>();

        public ICollection<Travel> EndingPoints { get; set; } = new List<Travel>();
    }
}
