﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data.Models
{
    public class TravelPreference
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int TravelId { get; set; }

        public Travel Travel { get; set; }

        [Required]
        public int PreferenceId { get; set; }

        public Preference Preference { get; set; }
    }
}
