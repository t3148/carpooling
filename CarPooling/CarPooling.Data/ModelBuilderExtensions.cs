﻿using CarPooling.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data
{
    public static class ModelBuilderExtensions
    {
        public static IEnumerable<City> Cities { get; set; }

        public static IEnumerable<Comment> Comments { get; set; }

        public static IEnumerable<Feedback> Feedbacks { get; set; }

        public static IEnumerable<Role> Roles { get; set; }

        public static IEnumerable<Status> Statuses { get; set; }

        public static IEnumerable<Travel> Travels { get; set; }

        public static IEnumerable<TravelPreference> TravelPreferences { get; set; }

        public static IEnumerable<Preference> Preferences { get; set; }

        public static IEnumerable<User> Users { get; set; }

        public static IEnumerable<UserRole> UserRoles { get; set; }

        public static IEnumerable<ApprovedPassenger> ApprovedPassengers { get; set; }

        public static IEnumerable<PendingPassenger> PendingPassengers { get; set; }

        static ModelBuilderExtensions()
        {
            Cities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Asenovgrad"
                },
                new City()
                {
                    Id = 2,
                    Name = "Bansko"
                },
                new City()
                {
                    Id = 3,
                    Name = "Blagoevgrad"
                },
                new City()
                {
                    Id = 4,
                    Name = "Bourgas"
                },
                new City()
                {
                    Id = 5,
                    Name = "Varna"
                },
                new City()
                {
                    Id = 6,
                    Name = "Veliko Tarnovo"
                },
                new City()
                {
                    Id = 7,
                    Name = "Velingrad"
                },
                new City()
                {
                    Id = 8,
                    Name = "Vidin"
                },
                new City()
                {
                    Id = 9,
                    Name = "Vraca"
                },
                new City()
                {
                    Id = 10,
                    Name = "Gabrovo"
                },
                new City()
                {
                    Id = 11,
                    Name = "Dimitrovgrad"
                },
                new City()
                {
                    Id = 12,
                    Name = "Etropole"
                },
                new City()
                {
                    Id = 13,
                    Name = "Karlovo"
                },
                new City()
                {
                    Id = 14,
                    Name = "Lovech"
                },
                new City()
                {
                    Id = 15,
                    Name = "Montana"
                },
                new City()
                {
                    Id = 16,
                    Name = "Pazardzhik"
                },
                new City()
                {
                    Id = 17,
                    Name = "Pernik"
                },
                new City()
                {
                    Id = 18,
                    Name = "Plovdiv"
                },
                new City()
                {
                    Id = 19,
                    Name = "Razgrad"
                },
                new City()
                {
                    Id = 20,
                    Name = "Ruse"
                },
                new City()
                {
                    Id = 21,
                    Name = "Svilengrad"
                },
                new City()
                {
                    Id = 22,
                    Name = "Sevlievo"
                },
                new City()
                {
                    Id = 23,
                    Name = "Sliven"
                },
                new City()
                {
                    Id = 24,
                    Name = "Sofia"
                },
                new City()
                {
                    Id = 25,
                    Name = "Stara Zagora"
                },
                new City()
                {
                    Id = 26,
                    Name = "Topolovgrad"
                },
                new City()
                {
                    Id = 27,
                    Name = "Troyan"
                },
                new City()
                {
                    Id = 28,
                    Name = "Targovishte"
                },
                new City()
                {
                    Id = 29,
                    Name = "Haskovo"
                },
                new City()
                {
                    Id = 30,
                    Name = "Yambol"
                }
            };

            Statuses = new List<Status>()
            {
                new Status()
                {
                    Id = 1,
                    Type = "Available"
                },
                new Status()
                {
                    Id = 2,
                    Type = "Completed"
                }
            };

            Roles = new List<Role>()
            {
                new Role()
                {
                    Id = 1,
                    Type = "User"
                },
                new Role()
                {
                    Id = 2,
                    Type = "Admin"
                }
            };

            Users = new List<User>()
            {
                new User()
                {
                    Id = 1,
                    Username = "bkehayov",
                    FirstName = "Bogdan",
                    LastName = "Kehayov",
                    Password = "TestPass-123",
                    Email = "bogdan.kehayov@gmail.com",
                    PhoneNumber = "0894747949",
                    ProfilePicture = "c504be68-ba0f-4a72-8314-b0b0c15df4cc_82647092_10213410429654656_7370158159039561728_n.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 2,
                    Username = "ataskov",
                    FirstName = "Alexander",
                    LastName = "Taskov",
                    Password = "TestPass-123",
                    Email = "alex.taskovv@gmail.com",
                    PhoneNumber = "0898322922",
                    ProfilePicture = "7b01f2ba-454d-40d4-b6be-8c4e2cf883c7_sahezarabota.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 3,
                    Username = "k.stanoev",
                    FirstName = "Kiril",
                    LastName = "Stanoev",
                    Password = "TestPass-123",
                    Email = "kiril.stanoev@yahoo.com",
                    PhoneNumber = "0899353878",
                    ProfilePicture = "cef26811-1f8e-4b8c-8cad-54d0e1622a0c_kstanoev.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 4,
                    Username = "p_raykov",
                    FirstName = "Petar",
                    LastName = "Raykov",
                    Password = "TestPass-123",
                    Email = "petar.raykov@abv.bg",
                    PhoneNumber = "0893564789",
                    ProfilePicture = "66ac03d5-6c4c-42cb-b5cf-189cb20423bd_praykov.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 5,
                    Username = "petrovv",
                    FirstName = "Ivan",
                    LastName = "Petrov",
                    Password = "TestPass-123",
                    Email = "i_petrov@mail.bg",
                    PhoneNumber = "0888930900",
                    ProfilePicture = "f33ec6a7-7a02-4e75-8cd3-c6445d47de06_face1.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 6,
                    Username = "mmarinova",
                    FirstName = "Mariya",
                    LastName = "Marinova",
                    Password = "TestPass-123",
                    Email = "marinova123@consulting.net",
                    PhoneNumber = "0897745093",
                    ProfilePicture = "6877f653-0c54-4f1b-829e-6eb44ecaef57_face2.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 7,
                    Username = "zayykov",
                    FirstName = "Georgi",
                    LastName = "Zaykov",
                    Password = "TestPass-123",
                    Email = "georgi@mail.bg",
                    PhoneNumber = "0898921812",
                    ProfilePicture = "af700f7c-8b22-4165-8b9b-9ba01b7de249_face3.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 8,
                    Username = "mike.dev",
                    FirstName = "Mike",
                    LastName = "Wazowski",
                    Password = "TestPass-123",
                    Email = "mikedev@gmail.com",
                    PhoneNumber = "0898123456",
                    ProfilePicture = "64da4c87-752f-4d07-80ac-42a5e59238b4_face4.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 9,
                    Username = "simeon911",
                    FirstName = "Simo",
                    LastName = "Ivaylov",
                    Password = "TestPass-123",
                    Email = "simo911@yahoo.com",
                    PhoneNumber = "0896882245",
                    ProfilePicture = "15da5fd0-132e-47ac-97a5-6e2772310f04_face5.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 10,
                    Username = "junior_dev",
                    FirstName = "Stefan",
                    LastName = "Stefanov",
                    Password = "TestPass-123",
                    Email = "sstefanov@gmail.com",
                    PhoneNumber = "0896823475",
                    ProfilePicture = "4addf827-4e6b-4280-8285-4a5fa4886a79_face6.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 11,
                    Username = "viksana",
                    FirstName = "Victor",
                    LastName = "Yordanov",
                    Password = "TestPass-123",
                    Email = "victor@gmail.com",
                    PhoneNumber = "0896825109",
                    ProfilePicture = "c7f485d1-3f1c-4356-ac54-a7cdeab8b167_face7.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 12,
                    Username = "milenа96",
                    FirstName = "Milenа",
                    LastName = "Stefanovа",
                    Password = "TestPass-123",
                    Email = "milenа@gmail.com",
                    PhoneNumber = "0883782104",
                    ProfilePicture = "d57065c9-80e5-4c1f-bb02-71ff5f92b6b7_face8.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 13,
                    Username = "djvascoc",
                    FirstName = "Vasil",
                    LastName = "Tsvyatkov",
                    Password = "TestPass-123",
                    Email = "vascoc@gmail.com",
                    PhoneNumber = "0889528888",
                    ProfilePicture = "c83fd0bc-9bbb-4dcc-9aa2-d46f8faf8837_face9.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 14,
                    Username = "ka_lin",
                    FirstName = "Kalin",
                    LastName = "Kalinov",
                    Password = "TestPass-123",
                    Email = "kkalinov@abv.bg",
                    PhoneNumber = "0887410410",
                    ProfilePicture = "d611aed2-6a0b-4d30-8bbd-7b7afd901b9b_face10.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                },
                new User()
                {
                    Id = 15,
                    Username = "dzhamulov",
                    FirstName = "Ivan",
                    LastName = "Dzhamulov",
                    Password = "TestPass-123",
                    Email = "dzhamulov@soft.com",
                    PhoneNumber = "0887555963",
                    ProfilePicture = "6e9f08d6-dcb4-4b20-b100-773b0a8b29fd_face11.jpg",
                    IsBlocked = false,
                    IsDeleted = false
                }
            };

            Travels = new List<Travel>()
            {
                new Travel()
                {
                    Id = 1,
                    StartingPointId = 1,
                    EndingPointId = 2,
                    DepartureTime = new DateTime(2022, 3, 15, 10, 30, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 3,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 2,
                    StartingPointId = 9,
                    EndingPointId = 10,
                    DepartureTime = new DateTime(2022, 1, 12, 15, 30, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 3,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 3,
                    StartingPointId = 3,
                    EndingPointId = 4,
                    DepartureTime = new DateTime(2021, 12, 15, 20, 30, 0),
                    FreeSeats = 2,
                    CreatedByUserId = 4,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 4,
                    StartingPointId = 5,
                    EndingPointId = 6,
                    DepartureTime = new DateTime(2021, 12, 18, 13, 30, 0),
                    FreeSeats = 4,
                    CreatedByUserId = 1,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 5,
                    StartingPointId = 7,
                    EndingPointId = 2,
                    DepartureTime = new DateTime(2022, 3, 15, 10, 30, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 1,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 6,
                    StartingPointId = 15,
                    EndingPointId = 13,
                    DepartureTime = new DateTime(2022, 2, 2, 9, 45, 0),
                    FreeSeats = 5,
                    CreatedByUserId = 2,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 7,
                    StartingPointId = 20,
                    EndingPointId = 18,
                    DepartureTime = new DateTime(2021, 12, 20, 18, 15, 0),
                    FreeSeats = 5,
                    CreatedByUserId = 7,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 8,
                    StartingPointId = 12,
                    EndingPointId = 19,
                    DepartureTime = new DateTime(2022, 1, 12, 11, 25, 0),
                    FreeSeats = 2,
                    CreatedByUserId = 2,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 9,
                    StartingPointId = 23,
                    EndingPointId = 6,
                    DepartureTime = new DateTime(2021, 12, 11, 21, 10, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 2,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 10,
                    StartingPointId = 29,
                    EndingPointId = 20,
                    DepartureTime = new DateTime(2021, 12, 15, 22, 00, 0),
                    FreeSeats = 4,
                    CreatedByUserId = 2,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 11,
                    StartingPointId = 27,
                    EndingPointId = 14,
                    DepartureTime = new DateTime(2022, 10, 18, 5, 50, 0),
                    FreeSeats = 6,
                    CreatedByUserId = 3,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 12,
                    StartingPointId = 17,
                    EndingPointId = 26,
                    DepartureTime = new DateTime(2021, 12, 14, 8, 50, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 3,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 13,
                    StartingPointId = 18,
                    EndingPointId = 13,
                    DepartureTime = new DateTime(2021, 3, 15, 22, 00, 0),
                    FreeSeats = 4,
                    CreatedByUserId = 2,
                    StatusId = 2,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 14,
                    StartingPointId = 20,
                    EndingPointId = 30,
                    DepartureTime = new DateTime(2021, 7, 18, 5, 50, 0),
                    FreeSeats = 6,
                    CreatedByUserId = 1,
                    StatusId = 2,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 15,
                    StartingPointId = 9,
                    EndingPointId = 7,
                    DepartureTime = new DateTime(2021, 8, 14, 8, 50, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 1,
                    StatusId = 2,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 16,
                    StartingPointId = 13,
                    EndingPointId = 17,
                    DepartureTime = new DateTime(2021, 3, 17, 22, 00, 0),
                    FreeSeats = 4,
                    CreatedByUserId = 2,
                    StatusId = 2,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 17,
                    StartingPointId = 5,
                    EndingPointId = 3,
                    DepartureTime = new DateTime(2021, 7, 19, 15, 50, 0),
                    FreeSeats = 6,
                    CreatedByUserId = 2,
                    StatusId = 2,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 18,
                    StartingPointId = 18,
                    EndingPointId = 17,
                    DepartureTime = new DateTime(2021, 6, 12, 18, 50, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 3,
                    StatusId = 2,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 19,
                    StartingPointId = 15,
                    EndingPointId = 23,
                    DepartureTime = new DateTime(2022, 7, 11, 17, 50, 0),
                    FreeSeats = 2,
                    CreatedByUserId = 1,
                    StatusId = 1,
                    IsDeleted = false
                },
                new Travel()
                {
                    Id = 20,
                    StartingPointId = 27,
                    EndingPointId = 3,
                    DepartureTime = new DateTime(2022, 6, 5, 8, 50, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 2,
                    StatusId = 1,
                    IsDeleted = false
                }
            };

            Preferences = new List<Preference>()
            {
                new Preference()
                {
                    Id = 1,
                    Text = "No smoking"
                },
                new Preference()
                {
                    Id = 2,
                    Text = "No luggage"
                },
                new Preference()
                {
                    Id = 3,
                    Text = "No sleeping"
                },
                new Preference()
                {
                    Id = 4,
                    Text = "Pet-friendly"
                },
                new Preference()
                {
                    Id = 5,
                    Text = "Air Conditioning"
                },
                new Preference()
                {
                    Id = 6,
                    Text = "Free music choice"
                },
                new Preference()
                {
                    Id = 7,
                    Text = "No political discussions"
                },
                new Preference()
                {
                    Id = 8,
                    Text = "No drinking"
                },
            };

            TravelPreferences = new List<TravelPreference>()
            {
                new TravelPreference()
                {
                    Id = 1,
                    TravelId = 1,
                    PreferenceId = 1
                },
                new TravelPreference()
                {
                    Id = 2,
                    TravelId = 1,
                    PreferenceId = 4
                },
                new TravelPreference()
                {
                    Id = 3,
                    TravelId = 1,
                    PreferenceId = 5
                },
                new TravelPreference()
                {
                    Id = 4,
                    TravelId = 2,
                    PreferenceId = 2
                },
                new TravelPreference()
                {
                    Id = 5,
                    TravelId = 3,
                    PreferenceId = 3
                },
                new TravelPreference()
                {
                    Id = 6,
                    TravelId = 3,
                    PreferenceId = 7
                },
                new TravelPreference()
                {
                    Id = 7,
                    TravelId = 1,
                    PreferenceId = 2
                },
                new TravelPreference()
                {
                    Id = 8,
                    TravelId = 1,
                    PreferenceId = 3
                },
                new TravelPreference()
                {
                    Id = 9,
                    TravelId = 1,
                    PreferenceId = 6
                },
                new TravelPreference()
                {
                    Id = 10,
                    TravelId = 2,
                    PreferenceId = 3
                },
                new TravelPreference()
                {
                    Id = 11,
                    TravelId = 3,
                    PreferenceId = 4
                },
                new TravelPreference()
                {
                    Id = 12,
                    TravelId = 3,
                    PreferenceId = 8
                },
                new TravelPreference()
                {
                    Id = 13,
                    TravelId = 6,
                    PreferenceId = 2
                },
                new TravelPreference()
                {
                    Id = 14,
                    TravelId = 6,
                    PreferenceId = 3
                },
                new TravelPreference()
                {
                    Id = 15,
                    TravelId = 6,
                    PreferenceId = 7
                },
                new TravelPreference()
                {
                    Id = 16,
                    TravelId = 7,
                    PreferenceId = 5
                },
                new TravelPreference()
                {
                    Id = 17,
                    TravelId = 7,
                    PreferenceId = 4
                },
                new TravelPreference()
                {
                    Id = 18,
                    TravelId = 7,
                    PreferenceId = 3
                },

            };

            UserRoles = new List<UserRole>()
            {
                new UserRole()
                {
                    Id = 1,
                    UserId = 1,
                    RoleId = 2
                },
                new UserRole()
                {
                    Id = 2,
                    UserId = 2,
                    RoleId = 2
                },
                new UserRole()
                {
                    Id = 3,
                    UserId = 3,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 4,
                    UserId = 4,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 5,
                    UserId = 5,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 6,
                    UserId = 6,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 7,
                    UserId = 7,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 8,
                    UserId = 8,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 9,
                    UserId = 9,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 10,
                    UserId = 10,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 11,
                    UserId = 1,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 12,
                    UserId = 2,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 13,
                    UserId = 11,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 14,
                    UserId = 12,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 15,
                    UserId = 13,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 16,
                    UserId = 14,
                    RoleId = 1
                },
                new UserRole()
                {
                    Id = 17,
                    UserId = 15,
                    RoleId = 1
                }
            };

            Feedbacks = new List<Feedback>()
            {
                new Feedback()
                {
                    Id = 1,
                    Rating = 3.9,
                    FromUserId = 3,
                    ToUserId = 4,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 2,
                    Rating = 2,
                    FromUserId = 3,
                    ToUserId = 4,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 3,
                    Rating = 3,
                    FromUserId = 3,
                    ToUserId = 5,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 4,
                    Rating = 4,
                    FromUserId = 5,
                    ToUserId = 4,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 5,
                    Rating = 5,
                    FromUserId = 6,
                    ToUserId = 7,
                    FromDriverToPassenger = true
                },

                new Feedback()
                {
                    Id = 6,
                    Rating = 2,
                    FromUserId = 8,
                    ToUserId = 9,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 7,
                    Rating = 5,
                    FromUserId = 8,
                    ToUserId = 9,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 8,
                    Rating = 3,
                    FromUserId = 9,
                    ToUserId = 10,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 9,
                    Rating = 4,
                    FromUserId = 9,
                    ToUserId = 10,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 10,
                    Rating = 4,
                    FromUserId = 10,
                    ToUserId = 11,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 11,
                    Rating = 5,
                    FromUserId = 10,
                    ToUserId = 11,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 12,
                    Rating = 5,
                    FromUserId = 11,
                    ToUserId = 12,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 13,
                    Rating = 4,
                    FromUserId = 11,
                    ToUserId = 12,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 14,
                    Rating = 4.7,
                    FromUserId = 12,
                    ToUserId = 13,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 15,
                    Rating = 3.2,
                    FromUserId = 12,
                    ToUserId = 13,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 16,
                    Rating = 4,
                    FromUserId = 13,
                    ToUserId = 14,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 17,
                    Rating = 3,
                    FromUserId = 13,
                    ToUserId = 14,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 18,
                    Rating = 5,
                    FromUserId = 14,
                    ToUserId = 15,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 19,
                    Rating = 3,
                    FromUserId = 14,
                    ToUserId = 15,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 20,
                    Rating = 1,
                    FromUserId = 15,
                    ToUserId = 1,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 21,
                    Rating = 2,
                    FromUserId = 15,
                    ToUserId = 1,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 22,
                    Rating = 4.3,
                    FromUserId = 15,
                    ToUserId = 2,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 23,
                    Rating = 3,
                    FromUserId = 15,
                    ToUserId = 2,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 24,
                    Rating = 4,
                    FromUserId = 13,
                    ToUserId = 5,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 25,
                    Rating = 5,
                    FromUserId = 13,
                    ToUserId = 5,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 26,
                    Rating = 1,
                    FromUserId = 12,
                    ToUserId = 9,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 27,
                    Rating = 2,
                    FromUserId = 12,
                    ToUserId = 9,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 28,
                    Rating = 3,
                    FromUserId = 3,
                    ToUserId = 8,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 29,
                    Rating = 4,
                    FromUserId = 9,
                    ToUserId = 6,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 30,
                    Rating = 3.9,
                    FromUserId = 6,
                    ToUserId = 8,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 31,
                    Rating = 4.3,
                    FromUserId = 9,
                    ToUserId = 3,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 32,
                    Rating = 5,
                    FromUserId = 6,
                    ToUserId = 4,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 33,
                    Rating = 5,
                    FromUserId = 9,
                    ToUserId = 4,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 34,
                    Rating = 5,
                    FromUserId = 6,
                    ToUserId = 3,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 35,
                    Rating = 5,
                    FromUserId = 7,
                    ToUserId = 1,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 36,
                    Rating = 4.5,
                    FromUserId = 14,
                    ToUserId = 1,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 37,
                    Rating = 4.6,
                    FromUserId = 13,
                    ToUserId = 5,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 38,
                    Rating = 3.8,
                    FromUserId = 11,
                    ToUserId = 5,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 39,
                    Rating = 4.3,
                    FromUserId = 7,
                    ToUserId = 7,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 40,
                    Rating = 3.9,
                    FromUserId = 14,
                    ToUserId = 7,
                    FromDriverToPassenger = true
                },
                new Feedback()
                {
                    Id = 41,
                    Rating = 4.3,
                    FromUserId = 6,
                    ToUserId = 8,
                    FromDriverToPassenger = false
                },
                new Feedback()
                {
                    Id = 42,
                    Rating = 2.8,
                    FromUserId = 3,
                    ToUserId = 8,
                    FromDriverToPassenger = true
                }

            };

            Comments = new List<Comment>()
            {
                new Comment()
                {
                    Id = 1,
                    Text = "Great passenger",
                    FeedbackId = 1,
                    IsDeleted = false
                },
                new Comment()
                {
                    Id = 2,
                    Text = "Bumpy ride",
                    FeedbackId = 4,
                    IsDeleted = false
                }
            };

            ApprovedPassengers = new List<ApprovedPassenger>()
            {
                new ApprovedPassenger()
                {
                    Id = 1,
                    TravelId = 1,
                    UserId = 2
                },
                new ApprovedPassenger()
                {
                    Id = 2,
                    TravelId = 2,
                    UserId = 11
                },
                new ApprovedPassenger()
                {
                    Id = 3,
                    TravelId = 4,
                    UserId = 2,
                },
                new ApprovedPassenger()
                {
                    Id = 4,
                    TravelId = 6,
                    UserId = 9
                },
                new ApprovedPassenger()
                {
                    Id = 5,
                    TravelId = 6,
                    UserId = 10
                },
                new ApprovedPassenger()
                {
                    Id = 6,
                    TravelId = 6,
                    UserId = 3,
                },
                new ApprovedPassenger()
                {
                    Id = 7,
                    TravelId = 7,
                    UserId = 7
                },
                new ApprovedPassenger()
                {
                    Id = 8,
                    TravelId = 7,
                    UserId = 11
                },
                new ApprovedPassenger()
                {
                    Id = 9,
                    TravelId = 7,
                    UserId = 3,
                }
            };

            PendingPassengers = new List<PendingPassenger>()
            {
                new PendingPassenger()
                {
                    Id = 1,
                    TravelId = 5,
                    UserId = 8
                },

                new PendingPassenger()
                {
                    Id = 2,
                    TravelId = 3,
                    UserId = 3
                },

                new PendingPassenger()
                {
                    Id = 3,
                    TravelId = 3,
                    UserId = 9
                },
                new PendingPassenger()
                {
                    Id = 4,
                    TravelId = 6,
                    UserId = 5
                },

                new PendingPassenger()
                {
                    Id = 5,
                    TravelId = 6,
                    UserId = 6
                },

                new PendingPassenger()
                {
                    Id = 6,
                    TravelId = 6,
                    UserId = 7
                },
                new PendingPassenger()
                {
                    Id = 7,
                    TravelId = 7,
                    UserId = 1
                },

                new PendingPassenger()
                {
                    Id = 8,
                    TravelId = 7,
                    UserId = 9
                },

                new PendingPassenger()
                {
                    Id = 9,
                    TravelId = 7,
                    UserId = 15
                }
            };
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().HasData(Cities);
            modelBuilder.Entity<Comment>().HasData(Comments);
            modelBuilder.Entity<Feedback>().HasData(Feedbacks);
            modelBuilder.Entity<Role>().HasData(Roles);
            modelBuilder.Entity<Status>().HasData(Statuses);
            modelBuilder.Entity<Travel>().HasData(Travels);
            modelBuilder.Entity<TravelPreference>().HasData(TravelPreferences);
            modelBuilder.Entity<Preference>().HasData(Preferences);
            modelBuilder.Entity<User>().HasData(Users);
            modelBuilder.Entity<UserRole>().HasData(UserRoles);
            modelBuilder.Entity<ApprovedPassenger>().HasData(ApprovedPassengers);
            modelBuilder.Entity<PendingPassenger>().HasData(PendingPassengers);
        }
    }
}
