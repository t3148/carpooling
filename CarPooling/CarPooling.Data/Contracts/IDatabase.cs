﻿using CarPooling.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data.Contracts
{
    public interface IDatabase
    {
        DbSet<City> Cities { get; set; }

        DbSet<Comment> Comments { get; set; }
        
        DbSet<Feedback> Feedbacks { get; set; }
        
        DbSet<Role> Roles { get; set; }
        
        DbSet<Status> Statuses { get; set; }
        
        DbSet<Travel> Travels { get; set; }
        
        DbSet<TravelPreference> TravelPreferences { get; set; }
        
        DbSet<User> Users { get; set; }
        
        DbSet<UserRole> UserRoles { get; set; }

    }
}
