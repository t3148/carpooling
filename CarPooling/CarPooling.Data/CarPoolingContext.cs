﻿using CarPooling.Data.Contracts;
using CarPooling.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Data
{
    public class CarPoolingContext : DbContext
    {
        public CarPoolingContext(DbContextOptions<CarPoolingContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Fluent API
            modelBuilder.Entity<Feedback>().HasOne(r => r.ToUser).WithMany(r => r.ReceivedFeedbacks).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Feedback>().HasOne(r => r.FromUser).WithMany(r => r.GivenFeedbacks).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Travel>().HasOne(t => t.CreatedByUser).WithMany(t => t.Travels).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Travel>().HasOne(t => t.StartingPoint).WithMany(t => t.StartingPoints).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Travel>().HasOne(t => t.EndingPoint).WithMany(t => t.EndingPoints).OnDelete(DeleteBehavior.Restrict);

            //Unique Entities
            modelBuilder.Entity<User>().HasIndex(c => c.Username).IsUnique();
            modelBuilder.Entity<User>().HasIndex(e => e.Email).IsUnique();
            modelBuilder.Entity<User>().HasIndex(e => e.PhoneNumber).IsUnique();

            modelBuilder.Seed();
        }

        public DbSet<City> Cities { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Feedback> Feedbacks { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Status> Statuses { get; set; }

        public DbSet<Travel> Travels { get; set; }

        public DbSet<TravelPreference> TravelPreferences { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<ApprovedPassenger> ApprovedPassengers { get; set; }

        public DbSet<PendingPassenger> PendingPassengers { get; set; }
    }
}
