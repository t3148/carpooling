﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CarPooling.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Preferences",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Preferences", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProfilePicture = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsBlocked = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Feedbacks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rating = table.Column<double>(type: "float", nullable: false),
                    FromUserId = table.Column<int>(type: "int", nullable: false),
                    ToUserId = table.Column<int>(type: "int", nullable: false),
                    FromDriverToPassenger = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feedbacks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Feedbacks_Users_FromUserId",
                        column: x => x.FromUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Feedbacks_Users_ToUserId",
                        column: x => x.ToUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Travels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartingPointId = table.Column<int>(type: "int", nullable: false),
                    EndingPointId = table.Column<int>(type: "int", nullable: false),
                    DepartureTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FreeSeats = table.Column<int>(type: "int", nullable: false),
                    CreatedByUserId = table.Column<int>(type: "int", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Travels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Travels_Cities_EndingPointId",
                        column: x => x.EndingPointId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Travels_Cities_StartingPointId",
                        column: x => x.StartingPointId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Travels_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Travels_Users_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FeedbackId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Feedbacks_FeedbackId",
                        column: x => x.FeedbackId,
                        principalTable: "Feedbacks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApprovedPassengers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TravelId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApprovedPassengers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApprovedPassengers_Travels_TravelId",
                        column: x => x.TravelId,
                        principalTable: "Travels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApprovedPassengers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PendingPassengers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TravelId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PendingPassengers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PendingPassengers_Travels_TravelId",
                        column: x => x.TravelId,
                        principalTable: "Travels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PendingPassengers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TravelPreferences",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TravelId = table.Column<int>(type: "int", nullable: false),
                    PreferenceId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TravelPreferences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TravelPreferences_Preferences_PreferenceId",
                        column: x => x.PreferenceId,
                        principalTable: "Preferences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TravelPreferences_Travels_TravelId",
                        column: x => x.TravelId,
                        principalTable: "Travels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Asenovgrad" },
                    { 30, "Yambol" },
                    { 28, "Targovishte" },
                    { 27, "Troyan" },
                    { 26, "Topolovgrad" },
                    { 25, "Stara Zagora" },
                    { 24, "Sofia" },
                    { 23, "Sliven" },
                    { 22, "Sevlievo" },
                    { 21, "Svilengrad" },
                    { 20, "Ruse" },
                    { 19, "Razgrad" },
                    { 18, "Plovdiv" },
                    { 17, "Pernik" },
                    { 16, "Pazardzhik" },
                    { 29, "Haskovo" },
                    { 14, "Lovech" },
                    { 15, "Montana" },
                    { 2, "Bansko" },
                    { 3, "Blagoevgrad" },
                    { 4, "Bourgas" },
                    { 5, "Varna" },
                    { 7, "Velingrad" },
                    { 6, "Veliko Tarnovo" },
                    { 9, "Vraca" },
                    { 10, "Gabrovo" },
                    { 11, "Dimitrovgrad" },
                    { 12, "Etropole" },
                    { 13, "Karlovo" },
                    { 8, "Vidin" }
                });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "IsDeleted", "Text" },
                values: new object[,]
                {
                    { 6, false, "Free music choice" },
                    { 8, false, "No drinking" },
                    { 5, false, "Air Conditioning" },
                    { 7, false, "No political discussions" },
                    { 3, false, "No sleeping" },
                    { 2, false, "No luggage" },
                    { 1, false, "No smoking" },
                    { 4, false, "Pet-friendly" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Type" },
                values: new object[,]
                {
                    { 1, "User" },
                    { 2, "Admin" }
                });

            migrationBuilder.InsertData(
                table: "Statuses",
                columns: new[] { "Id", "Type" },
                values: new object[,]
                {
                    { 1, "Available" },
                    { 2, "Completed" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email", "FirstName", "IsBlocked", "IsDeleted", "LastName", "Password", "PhoneNumber", "ProfilePicture", "Username" },
                values: new object[,]
                {
                    { 13, "vascoc@gmail.com", "Vasil", false, false, "Tsvyatkov", "TestPass-123", "0889528888", "c83fd0bc-9bbb-4dcc-9aa2-d46f8faf8837_face9.jpg", "djvascoc" },
                    { 12, "milenа@gmail.com", "Milenа", false, false, "Stefanovа", "TestPass-123", "0883782104", "d57065c9-80e5-4c1f-bb02-71ff5f92b6b7_face8.jpg", "milenа96" },
                    { 11, "victor@gmail.com", "Victor", false, false, "Yordanov", "TestPass-123", "0896825109", "c7f485d1-3f1c-4356-ac54-a7cdeab8b167_face7.jpg", "viksana" },
                    { 10, "sstefanov@gmail.com", "Stefan", false, false, "Stefanov", "TestPass-123", "0896823475", "4addf827-4e6b-4280-8285-4a5fa4886a79_face6.jpg", "junior_dev" },
                    { 9, "simo911@yahoo.com", "Simo", false, false, "Ivaylov", "TestPass-123", "0896882245", "15da5fd0-132e-47ac-97a5-6e2772310f04_face5.jpg", "simeon911" },
                    { 8, "mikedev@gmail.com", "Mike", false, false, "Wazowski", "TestPass-123", "0898123456", "64da4c87-752f-4d07-80ac-42a5e59238b4_face4.jpg", "mike.dev" },
                    { 4, "petar.raykov@abv.bg", "Petar", false, false, "Raykov", "TestPass-123", "0893564789", "66ac03d5-6c4c-42cb-b5cf-189cb20423bd_praykov.jpg", "p_raykov" },
                    { 6, "marinova123@consulting.net", "Mariya", false, false, "Marinova", "TestPass-123", "0897745093", "6877f653-0c54-4f1b-829e-6eb44ecaef57_face2.jpg", "mmarinova" },
                    { 5, "i_petrov@mail.bg", "Ivan", false, false, "Petrov", "TestPass-123", "0888930900", "f33ec6a7-7a02-4e75-8cd3-c6445d47de06_face1.jpg", "petrovv" },
                    { 3, "kiril.stanoev@yahoo.com", "Kiril", false, false, "Stanoev", "TestPass-123", "0899353878", "cef26811-1f8e-4b8c-8cad-54d0e1622a0c_kstanoev.jpg", "k.stanoev" },
                    { 2, "alex.taskovv@gmail.com", "Alexander", false, false, "Taskov", "TestPass-123", "0898322922", "7b01f2ba-454d-40d4-b6be-8c4e2cf883c7_sahezarabota.jpg", "ataskov" },
                    { 1, "bogdan.kehayov@gmail.com", "Bogdan", false, false, "Kehayov", "TestPass-123", "0894747949", "c504be68-ba0f-4a72-8314-b0b0c15df4cc_82647092_10213410429654656_7370158159039561728_n.jpg", "bkehayov" },
                    { 14, "kkalinov@abv.bg", "Kalin", false, false, "Kalinov", "TestPass-123", "0887410410", "d611aed2-6a0b-4d30-8bbd-7b7afd901b9b_face10.jpg", "ka_lin" },
                    { 7, "georgi@mail.bg", "Georgi", false, false, "Zaykov", "TestPass-123", "0898921812", "af700f7c-8b22-4165-8b9b-9ba01b7de249_face3.jpg", "zayykov" },
                    { 15, "dzhamulov@soft.com", "Ivan", false, false, "Dzhamulov", "TestPass-123", "0887555963", "6e9f08d6-dcb4-4b20-b100-773b0a8b29fd_face11.jpg", "dzhamulov" }
                });

            migrationBuilder.InsertData(
                table: "Feedbacks",
                columns: new[] { "Id", "FromDriverToPassenger", "FromUserId", "IsDeleted", "Rating", "ToUserId" },
                values: new object[,]
                {
                    { 30, true, 6, false, 3.8999999999999999, 8 },
                    { 1, true, 3, false, 3.8999999999999999, 4 },
                    { 2, false, 3, false, 2.0, 4 },
                    { 8, true, 9, false, 3.0, 10 },
                    { 33, false, 9, false, 5.0, 4 },
                    { 3, true, 3, false, 3.0, 5 },
                    { 4, false, 5, false, 4.0, 4 },
                    { 31, false, 9, false, 4.2999999999999998, 3 },
                    { 32, true, 6, false, 5.0, 4 },
                    { 34, true, 6, false, 5.0, 3 },
                    { 29, false, 9, false, 4.0, 6 },
                    { 5, true, 6, false, 5.0, 7 },
                    { 35, false, 7, false, 5.0, 1 },
                    { 39, false, 7, false, 4.2999999999999998, 7 },
                    { 7, false, 8, false, 5.0, 9 },
                    { 6, true, 8, false, 2.0, 9 },
                    { 28, true, 3, false, 3.0, 8 },
                    { 23, true, 15, false, 3.0, 2 },
                    { 9, false, 9, false, 4.0, 10 },
                    { 10, true, 10, false, 4.0, 11 },
                    { 11, false, 10, false, 5.0, 11 },
                    { 38, true, 11, false, 3.7999999999999998, 5 },
                    { 22, false, 15, false, 4.2999999999999998, 2 },
                    { 21, false, 15, false, 2.0, 1 },
                    { 20, true, 15, false, 1.0, 1 },
                    { 19, false, 14, false, 3.0, 15 },
                    { 18, true, 14, false, 5.0, 15 },
                    { 40, true, 14, false, 3.8999999999999999, 7 },
                    { 36, true, 14, false, 4.5, 1 },
                    { 17, false, 13, false, 3.0, 14 },
                    { 41, false, 6, false, 4.2999999999999998, 8 },
                    { 16, true, 13, false, 4.0, 14 },
                    { 25, true, 13, false, 5.0, 5 },
                    { 24, false, 13, false, 4.0, 5 },
                    { 15, true, 12, false, 3.2000000000000002, 13 },
                    { 14, false, 12, false, 4.7000000000000002, 13 },
                    { 27, false, 12, false, 2.0, 9 },
                    { 26, true, 12, false, 1.0, 9 },
                    { 13, true, 11, false, 4.0, 12 },
                    { 12, false, 11, false, 5.0, 12 },
                    { 37, false, 13, false, 4.5999999999999996, 5 },
                    { 42, true, 3, false, 2.7999999999999998, 8 }
                });

            migrationBuilder.InsertData(
                table: "Travels",
                columns: new[] { "Id", "CreatedByUserId", "DepartureTime", "EndingPointId", "FreeSeats", "IsDeleted", "StartingPointId", "StatusId" },
                values: new object[,]
                {
                    { 4, 1, new DateTime(2021, 12, 18, 13, 30, 0, 0, DateTimeKind.Unspecified), 6, 4, false, 5, 1 },
                    { 11, 3, new DateTime(2022, 10, 18, 5, 50, 0, 0, DateTimeKind.Unspecified), 14, 6, false, 27, 1 },
                    { 16, 2, new DateTime(2021, 3, 17, 22, 0, 0, 0, DateTimeKind.Unspecified), 17, 4, false, 13, 2 },
                    { 17, 2, new DateTime(2021, 7, 19, 15, 50, 0, 0, DateTimeKind.Unspecified), 3, 6, false, 5, 2 },
                    { 20, 2, new DateTime(2022, 6, 5, 8, 50, 0, 0, DateTimeKind.Unspecified), 3, 3, false, 27, 1 },
                    { 9, 2, new DateTime(2021, 12, 11, 21, 10, 0, 0, DateTimeKind.Unspecified), 6, 3, false, 23, 1 },
                    { 8, 2, new DateTime(2022, 1, 12, 11, 25, 0, 0, DateTimeKind.Unspecified), 19, 2, false, 12, 1 },
                    { 6, 2, new DateTime(2022, 2, 2, 9, 45, 0, 0, DateTimeKind.Unspecified), 13, 5, false, 15, 1 },
                    { 1, 3, new DateTime(2022, 3, 15, 10, 30, 0, 0, DateTimeKind.Unspecified), 2, 3, false, 1, 1 },
                    { 2, 3, new DateTime(2022, 1, 12, 15, 30, 0, 0, DateTimeKind.Unspecified), 10, 3, false, 9, 1 },
                    { 13, 2, new DateTime(2021, 3, 15, 22, 0, 0, 0, DateTimeKind.Unspecified), 13, 4, false, 18, 2 },
                    { 10, 2, new DateTime(2021, 12, 15, 22, 0, 0, 0, DateTimeKind.Unspecified), 20, 4, false, 29, 1 },
                    { 18, 3, new DateTime(2021, 6, 12, 18, 50, 0, 0, DateTimeKind.Unspecified), 17, 3, false, 18, 2 },
                    { 3, 4, new DateTime(2021, 12, 15, 20, 30, 0, 0, DateTimeKind.Unspecified), 4, 2, false, 3, 1 },
                    { 19, 1, new DateTime(2022, 7, 11, 17, 50, 0, 0, DateTimeKind.Unspecified), 23, 2, false, 15, 1 },
                    { 15, 1, new DateTime(2021, 8, 14, 8, 50, 0, 0, DateTimeKind.Unspecified), 7, 3, false, 9, 2 },
                    { 14, 1, new DateTime(2021, 7, 18, 5, 50, 0, 0, DateTimeKind.Unspecified), 30, 6, false, 20, 2 },
                    { 5, 1, new DateTime(2022, 3, 15, 10, 30, 0, 0, DateTimeKind.Unspecified), 2, 3, false, 7, 1 },
                    { 7, 7, new DateTime(2021, 12, 20, 18, 15, 0, 0, DateTimeKind.Unspecified), 18, 5, false, 20, 1 },
                    { 12, 3, new DateTime(2021, 12, 14, 8, 50, 0, 0, DateTimeKind.Unspecified), 26, 3, false, 17, 1 }
                });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "Id", "RoleId", "UserId" },
                values: new object[,]
                {
                    { 11, 1, 1 },
                    { 16, 1, 14 },
                    { 1, 2, 1 },
                    { 7, 1, 7 },
                    { 14, 1, 12 },
                    { 8, 1, 8 },
                    { 2, 2, 2 },
                    { 12, 1, 2 },
                    { 13, 1, 11 },
                    { 10, 1, 10 },
                    { 3, 1, 3 },
                    { 9, 1, 9 },
                    { 4, 1, 4 },
                    { 5, 1, 5 },
                    { 6, 1, 6 },
                    { 15, 1, 13 },
                    { 17, 1, 15 }
                });

            migrationBuilder.InsertData(
                table: "ApprovedPassengers",
                columns: new[] { "Id", "TravelId", "UserId" },
                values: new object[,]
                {
                    { 3, 4, 2 },
                    { 9, 7, 3 },
                    { 8, 7, 11 },
                    { 7, 7, 7 },
                    { 1, 1, 2 },
                    { 2, 2, 11 },
                    { 6, 6, 3 },
                    { 4, 6, 9 },
                    { 5, 6, 10 }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "FeedbackId", "IsDeleted", "Text" },
                values: new object[,]
                {
                    { 1, 1, false, "Great passenger" },
                    { 2, 4, false, "Bumpy ride" }
                });

            migrationBuilder.InsertData(
                table: "PendingPassengers",
                columns: new[] { "Id", "TravelId", "UserId" },
                values: new object[,]
                {
                    { 9, 7, 15 },
                    { 8, 7, 9 },
                    { 7, 7, 1 },
                    { 1, 5, 8 },
                    { 3, 3, 9 },
                    { 2, 3, 3 },
                    { 4, 6, 5 },
                    { 5, 6, 6 },
                    { 6, 6, 7 }
                });

            migrationBuilder.InsertData(
                table: "TravelPreferences",
                columns: new[] { "Id", "PreferenceId", "TravelId" },
                values: new object[,]
                {
                    { 4, 2, 2 },
                    { 16, 5, 7 },
                    { 13, 2, 6 },
                    { 14, 3, 6 },
                    { 15, 7, 6 },
                    { 1, 1, 1 },
                    { 2, 4, 1 },
                    { 17, 4, 7 },
                    { 3, 5, 1 },
                    { 11, 4, 3 },
                    { 6, 7, 3 },
                    { 5, 3, 3 },
                    { 7, 2, 1 },
                    { 8, 3, 1 },
                    { 9, 6, 1 },
                    { 10, 3, 2 },
                    { 12, 8, 3 },
                    { 18, 3, 7 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApprovedPassengers_TravelId",
                table: "ApprovedPassengers",
                column: "TravelId");

            migrationBuilder.CreateIndex(
                name: "IX_ApprovedPassengers_UserId",
                table: "ApprovedPassengers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_FeedbackId",
                table: "Comments",
                column: "FeedbackId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Feedbacks_FromUserId",
                table: "Feedbacks",
                column: "FromUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Feedbacks_ToUserId",
                table: "Feedbacks",
                column: "ToUserId");

            migrationBuilder.CreateIndex(
                name: "IX_PendingPassengers_TravelId",
                table: "PendingPassengers",
                column: "TravelId");

            migrationBuilder.CreateIndex(
                name: "IX_PendingPassengers_UserId",
                table: "PendingPassengers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TravelPreferences_PreferenceId",
                table: "TravelPreferences",
                column: "PreferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_TravelPreferences_TravelId",
                table: "TravelPreferences",
                column: "TravelId");

            migrationBuilder.CreateIndex(
                name: "IX_Travels_CreatedByUserId",
                table: "Travels",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Travels_EndingPointId",
                table: "Travels",
                column: "EndingPointId");

            migrationBuilder.CreateIndex(
                name: "IX_Travels_StartingPointId",
                table: "Travels",
                column: "StartingPointId");

            migrationBuilder.CreateIndex(
                name: "IX_Travels_StatusId",
                table: "Travels",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_UserId",
                table: "UserRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_PhoneNumber",
                table: "Users",
                column: "PhoneNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_Username",
                table: "Users",
                column: "Username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApprovedPassengers");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "PendingPassengers");

            migrationBuilder.DropTable(
                name: "TravelPreferences");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "Feedbacks");

            migrationBuilder.DropTable(
                name: "Preferences");

            migrationBuilder.DropTable(
                name: "Travels");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
