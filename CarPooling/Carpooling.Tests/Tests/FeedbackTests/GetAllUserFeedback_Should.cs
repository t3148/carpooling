﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.FeedbackTests
{
    [TestClass]
    public class GetAllUserFeedback_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnAllCorrectUserFeedback_When_ParamsAreValid()
        {
            var expected = new FeedbackPresentDTO
            {
                Rating = 3.9,
                FromUser = "Kiril Stanoev",
                ToUser = "Petar Raykov",
                Comment = "Great passenger",
            };

            var modelMapper = new Mock<ModelMapper>();

            var userId = 4;

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                var actualFeedbacks = await sut.GetAllUserFeedbackAsync(userId);

                var actual = actualFeedbacks.First();

                Assert.AreEqual(expected.Comment, actual.Comment);
                Assert.AreEqual(expected.FromUser, actual.FromUser);
                Assert.AreEqual(expected.Rating, actual.Rating);
                Assert.AreEqual(expected.ToUser, actual.ToUser);
                
            }
        }

        [TestMethod]
        public void ThrowException_When_NoFeedbacksFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.GetAllUserFeedbackAsync(int.MaxValue));
            }
        }
    }
}
