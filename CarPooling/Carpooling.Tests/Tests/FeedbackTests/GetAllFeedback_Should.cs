﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.FeedbackTests
{
    [TestClass]
    public class GetAllFeedback_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectFeedbacks()
        {
            var expected = new FeedbackPresentDTO
            {
                Rating = 3.9,
                FromUser = "Kiril Stanoev",
                ToUser = "Petar Raykov",
                Comment = "Great passenger"
            };

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                var feedbacks = await sut.GetAllFeedbackAsync();

                var actual = feedbacks.First();

                Assert.AreEqual(expected.Rating, actual.Rating);
                Assert.AreEqual(expected.FromUser, actual.FromUser);
                Assert.AreEqual(expected.ToUser, actual.ToUser);
                Assert.AreEqual(expected.Comment, actual.Comment);
            }
        }
    }
}
