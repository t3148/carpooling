﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.FeedbackTests
{
    [TestClass]
    public class CreateFeedback_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.SaveChanges();
            }
        }

        

        [TestMethod]
        public async Task CreateFeedback_When_ParamsAreValid()
        {
            var modelMapper = new Mock<ModelMapper>();

            var travelId = 13;

            var expected = new FeedbackPresentDTO
            {
                Rating = 3,
                FromUser = "Ivan Petrov",
                ToUser = "Alexander Taskov",
                Comment = "Tres gucci"
            };

            var feedbackCreateDTO = new FeedbackCreateDTO
            {
                Rating = 3,
                FromUserId = 5,
                ToUserId = 2,
                Comment = "Tres gucci"
            };

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                var actual = await sut.CreateAsync(feedbackCreateDTO, travelId);

                Assert.AreEqual(expected.Rating, actual.Rating);
                Assert.AreEqual(expected.FromUser, actual.FromUser);
                Assert.AreEqual(expected.ToUser, actual.ToUser);
                Assert.AreEqual(expected.Comment, actual.Comment);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_PassengerAndDriversDoNotMatch()
        {
            var modelMapper = new Mock<ModelMapper>();

            var travelId = 13;

            var expected = new FeedbackPresentDTO
            {
                Rating = 3,
                FromUser = "Alexander Taskov",
                ToUser = "Bogdan Kehayov",
                Comment = "Tres gucci"
            };

            var feedbackCreateDTO = new FeedbackCreateDTO
            {
                Rating = 3,
                FromUserId = 2,
                ToUserId = 1,
                Comment = "Tres gucci"
            };

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.CreateAsync(feedbackCreateDTO, travelId));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            var feedbackCreateDTO = new FeedbackCreateDTO
            {
                Rating = 3,
                FromUserId = 1,
                ToUserId = 2,
                Comment = "Tres gucci"
            };

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.CreateAsync(feedbackCreateDTO, int.MaxValue));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelNotCompleted()
        {
            var modelMapper = new Mock<ModelMapper>();

            var travelId = 1;

            var feedbackCreateDTO = new FeedbackCreateDTO
            {
                Rating = 3,
                FromUserId = 1,
                ToUserId = 2,
                Comment = "Tres gucci"
            };

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await sut.CreateAsync(feedbackCreateDTO, travelId));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_PassengerNotApproved()
        {
            var modelMapper = new Mock<ModelMapper>();

            var travelId = 4;

            var feedbackCreateDTO = new FeedbackCreateDTO
            {
                Rating = 3,
                FromUserId = 7,
                ToUserId = 6,
                Comment = "Tres gucci"
            };

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await sut.CreateAsync(feedbackCreateDTO, travelId));
            }
        }
    }
}
