﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.FeedbackTests
{
    [TestClass]
    public class UpdateFeedback_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task UpdateFeedbackCorrectly_When_ParamsAreValid()
        {
            var modelMapper = new Mock<ModelMapper>();

            var feedbackId = 1;

            var expected = new FeedbackPresentDTO
            {
                Rating = 3,
                FromUser = "Kiril Stanoev",
                ToUser = "Petar Raykov",
                Comment = "Tres gucci"
            };

            var feedbackCreateDTO = new FeedbackCreateDTO
            {
                Rating = 3,
                FromUserId = 3,
                ToUserId = 4,
                Comment = "Tres gucci"
            };

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                var actual = await sut.UpdateAsync(feedbackId, feedbackCreateDTO);

                Assert.AreEqual(expected.Rating, actual.Rating);
                Assert.AreEqual(expected.FromUser, actual.FromUser);
                Assert.AreEqual(expected.ToUser, actual.ToUser);
                Assert.AreEqual(expected.Comment, actual.Comment);
            }
        }

        [TestMethod]
        public async Task CreateCommentForFeedback_When_ParamsAreValid()
        {
            var modelMapper = new Mock<ModelMapper>();

            var feedbackId = 3;

            var expected = new FeedbackPresentDTO
            {
                Rating = 3,
                FromUser = "Kiril Stanoev",
                ToUser = "Petar Raykov",
                Comment = "Tres gucci"
            };

            var feedbackCreateDTO = new FeedbackCreateDTO
            {
                Rating = 3,
                FromUserId = 3,
                ToUserId = 4,
                Comment = "Tres gucci"
            };

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                var actual = await sut.UpdateAsync(feedbackId, feedbackCreateDTO);

                Assert.AreEqual(expected.Rating, actual.Rating);
                Assert.AreEqual(expected.FromUser, actual.FromUser);
                Assert.AreEqual(expected.ToUser, actual.ToUser);
                Assert.AreEqual(expected.Comment, actual.Comment);
            }
        }

        [TestMethod]
        public void ThrowException_When_FeedbackNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            var feedbackDTO = new FeedbackCreateDTO();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.UpdateAsync(int.MaxValue, feedbackDTO));
            }
        }
    }
}
