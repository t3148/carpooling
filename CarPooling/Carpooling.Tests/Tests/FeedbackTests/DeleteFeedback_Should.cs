﻿using CarPooling.Data;
using CarPooling.Data.Models;
using CarPooling.Services.Common;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.FeedbackTests
{
    [TestClass]
    public class DeleteFeedback_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task DeleteFeedback_When_ParamsAreValid()
        {
            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);
                var isDeleted = await sut.DeleteAsync(1);
                Assert.IsTrue(isDeleted);
                var feedback = await assertContext.Feedbacks.FindAsync(1);
                Assert.IsTrue(feedback.IsDeleted);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_FeedbackNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new FeedbackService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.DeleteAsync(int.MaxValue));
            }
        }
    }
}
