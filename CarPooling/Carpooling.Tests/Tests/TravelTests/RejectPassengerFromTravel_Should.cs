﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.TravelTests
{
    [TestClass]
    public class RejectPassengerFromTravel_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.Statuses.AddRange(Utils.GetStatuses());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.Preferences.AddRange(Utils.GetPreferences());
                arrangeContext.TravelPreferences.AddRange(Utils.GetTravelPreferences());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task RejectCorrectly_When_ParamsAreValid()
        {
            var userId = 3;

            var travelId = 3;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await sut.DeclinePassengerFromTravel(userId, travelId);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            var userId = 3;

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.DeclinePassengerFromTravel(userId, int.MaxValue));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            var travelId = 3;

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.DeclinePassengerFromTravel(int.MaxValue, travelId));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelIsCompleted()
        {
            var modelMapper = new Mock<ModelMapper>();

            var travelId = 13;

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<ApplyForTravelException>(async () => await sut.DeclinePassengerFromTravel(int.MaxValue, travelId));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NoCandidates()
        {
            var modelMapper = new Mock<ModelMapper>();

            var userId = 1;

            var travelId = 3;

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.DeclinePassengerFromTravel(userId, travelId));
            }
        }
    }
}
