﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.TravelTests
{
    [TestClass]
    public class ListPendingPassengers_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.Statuses.AddRange(Utils.GetStatuses());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.Preferences.AddRange(Utils.GetPreferences());
                arrangeContext.TravelPreferences.AddRange(Utils.GetTravelPreferences());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnUsers_When_ParamsAreValid()
        {
            var expectedUsers = new List<UserPresentDTO>
            {
                new UserPresentDTO()
                {
                    Username = "k.stanoev",
                    FirstName = "Kiril",
                    LastName = "Stanoev",
                    Email = "kiril.stanoev@yahoo.com",
                    PhoneNumber ="0899353878",
                    ProfilePicture = "cef26811-1f8e-4b8c-8cad-54d0e1622a0c_kstanoev.jpg"
                },
                new UserPresentDTO()
                {
                    Username = "simeon911",
                    FirstName = "Simo",
                    LastName = "Ivaylov",
                    Email = "simo911@yahoo.com",
                    PhoneNumber = "0896882245",
                    ProfilePicture = "15da5fd0-132e-47ac-97a5-6e2772310f04_face5.jpg"
                }
            };

            var travelId = 3;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                var users = await sut.ListPendingPassengersForTravel(travelId);

                var actualUsers = users.ToList();

                for (int i = 0; i < actualUsers.Count; i++)
                {
                    var expected = expectedUsers[i];
                    var actual = actualUsers[i];

                    Assert.AreEqual(expected.Username, actual.Username);
                    Assert.AreEqual(expected.FirstName, actual.FirstName);
                    Assert.AreEqual(expected.LastName, actual.LastName);
                    Assert.AreEqual(expected.Email, actual.Email);
                    Assert.AreEqual(expected.PhoneNumber, actual.PhoneNumber);
                    Assert.AreEqual(expected.ProfilePicture, actual.ProfilePicture);
                }

            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.ListPendingPassengersForTravel(int.MaxValue));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelIsCompleted()
        {
            var modelMapper = new Mock<ModelMapper>();

            var travelId = 13;

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<ApplyForTravelException>(async () => await sut.ListPendingPassengersForTravel(travelId));
            }
        }
    }
}
