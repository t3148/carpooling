﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.TravelTests
{
    [TestClass]
    public class FilterByMultipleCriteria_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.Statuses.AddRange(Utils.GetStatuses());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.Preferences.AddRange(Utils.GetPreferences());
                arrangeContext.TravelPreferences.AddRange(Utils.GetTravelPreferences());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void ReturnCorrectTravels_When_ParamsAreValid()
        {
            var listOfTravels = new List<TravelPresentDTO>()
            {
                new TravelPresentDTO()
                {
                    StartingPoint = "Plovdiv",
                    EndingPoint = "Sofia",
                    DepartureTime = new DateTime(2022, 3, 15, 10, 30, 0),
                    FreeSeats = 3,
                    CreatedByUser = "Kiril Stanoev",
                },

                new TravelPresentDTO()
                {
                    StartingPoint = "Stara Zagora",
                    EndingPoint = "Nova Zagora",
                    DepartureTime = new DateTime(2022, 3, 15, 10, 30, 0),
                    FreeSeats = 3,
                    CreatedByUserId = 3,
                }
            };

            var startingCity = "Plovdiv";
            var endingCity = "Sofia";
            var departureTime = new DateTime(2022, 3, 15, 10, 30, 0);
            var user = "Kiril Stanoev";
            var seats = "3";

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                var filteredTravels = sut.FilterByMultipleCriteria(listOfTravels, startingCity, endingCity, user, departureTime, seats, default, default).Take(1);

                var expected = listOfTravels.First();
                var actual = filteredTravels.First();

                Assert.AreEqual(expected.StartingPoint, actual.StartingPoint);
                Assert.AreEqual(expected.EndingPoint, actual.EndingPoint);
                Assert.AreEqual(expected.DepartureTime, actual.DepartureTime);
                Assert.AreEqual(expected.FreeSeats, actual.FreeSeats);
                Assert.AreEqual(expected.CreatedByUser, actual.CreatedByUser);
                Assert.AreEqual(expected.Status, actual.Status);
            }
        }
    }
}
