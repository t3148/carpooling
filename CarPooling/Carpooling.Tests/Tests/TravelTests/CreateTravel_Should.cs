﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.TravelTests
{
    [TestClass]
    public class CreateTravel_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.Statuses.AddRange(Utils.GetStatuses());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.Preferences.AddRange(Utils.GetPreferences());
                arrangeContext.TravelPreferences.AddRange(Utils.GetTravelPreferences());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task CreateCorrectly_When_ParamsAreValid()
        {
            var expected = new TravelPresentDTO()
            {
                StartingPoint = "Asenovgrad",
                EndingPoint = "Asenovgrad",
                DepartureTime = DateTime.Parse("2022-03-15T10:30:00"),
                FreeSeats = 3,
                CreatedByUser = "Bogdan Kehayov",
                Status = "Available",
            };

            var travelToCreate = new TravelCreateDTO()
            {
                StartingPointId = 1,
                EndingPointId = 1,
                DepartureTime = DateTime.Parse("2022-03-15T10:30:00"),
                FreeSeats = 3,
            };


            var userId = 1;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                var actual = await sut.CreateAsync(userId, travelToCreate);

                Assert.AreEqual(expected.StartingPoint, actual.StartingPoint);
                Assert.AreEqual(expected.EndingPoint, actual.EndingPoint);
                Assert.AreEqual(expected.DepartureTime, actual.DepartureTime);
                Assert.AreEqual(expected.FreeSeats, actual.FreeSeats);
                Assert.AreEqual(expected.CreatedByUser, actual.CreatedByUser);
                Assert.AreEqual(expected.Status, actual.Status);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserNotFound()
        {
            var travelToCreate = new TravelCreateDTO();

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.CreateAsync(int.MaxValue, travelToCreate));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserBlocked()
        {
            var travelToCreate = new TravelCreateDTO();

            var userId = 1;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                var helper = new UserService(assertContext, modelMapper.Object);

                await helper.BlockUserAsync(userId);

                await Assert.ThrowsExceptionAsync<BlockedStateException>(async () => await sut.CreateAsync(userId, travelToCreate));
            }
        }
    }
}
