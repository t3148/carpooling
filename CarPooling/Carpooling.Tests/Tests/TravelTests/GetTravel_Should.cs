﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.TravelTests
{
    [TestClass]
    public class GetTravel_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.Statuses.AddRange(Utils.GetStatuses());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.Preferences.AddRange(Utils.GetPreferences());
                arrangeContext.TravelPreferences.AddRange(Utils.GetTravelPreferences());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectTravel_When_ParamsAreValid()
        {
            var expected = new TravelPresentDTO
            {
                StartingPoint = "Asenovgrad",
                EndingPoint = "Bansko",
                DepartureTime = DateTime.Parse("2022-03-15T10:30:00"),
                FreeSeats = 3,
                CreatedByUser = "Kiril Stanoev",
                Status = "Available",
                TravelPreferences = new string[] {
                      "No smoking",
                      "Pet-friendly",
                      "Air Conditioning",
                      "No luggage",
                      "No sleeping",
                      "Free music choice"
                }
            };

            var travelId = 1;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                var actual = await sut.GetAsync(travelId);

                Assert.AreEqual(expected.StartingPoint, actual.StartingPoint);
                Assert.AreEqual(expected.EndingPoint, actual.EndingPoint);
                Assert.AreEqual(expected.DepartureTime, actual.DepartureTime);
                Assert.AreEqual(expected.FreeSeats, actual.FreeSeats);
                Assert.AreEqual(expected.CreatedByUser, actual.CreatedByUser);
                Assert.AreEqual(expected.Status, actual.Status);
                CollectionAssert.AreEquivalent(expected.TravelPreferences.ToArray(), actual.TravelPreferences.ToArray());
            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.GetAsync(int.MaxValue));
            }
        }
    }
}
