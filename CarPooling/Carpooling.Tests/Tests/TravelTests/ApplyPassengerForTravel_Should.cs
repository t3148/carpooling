﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.TravelTests
{
    [TestClass]
    public class ApplyPassengerForTravel_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.Statuses.AddRange(Utils.GetStatuses());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.Preferences.AddRange(Utils.GetPreferences());
                arrangeContext.TravelPreferences.AddRange(Utils.GetTravelPreferences());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ApplyCorrectly_When_ParamsAreValid()
        {
            var travelId = 1;

            var userId = 4;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await sut.ApplyUserForTravel(travelId, userId);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelNotFound()
        {
            var userId = 1;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.ApplyUserForTravel(int.MaxValue, userId));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelIsCompleted()
        {
            var userId = 1;

            var travelId = 13;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<ApplyForTravelException>(async () => await sut.ApplyUserForTravel(travelId, userId));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_NoFreeSeats()
        {
            var travelToCreate = new TravelCreateDTO()
            {
                StartingPointId = 1,
                EndingPointId = 1,
                DepartureTime = DateTime.Parse("2022-03-15T10:30:00"),
                FreeSeats = 0,
            };

            var userId = 1;

            var travelId = 21;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await sut.CreateAsync(3, travelToCreate);

                await Assert.ThrowsExceptionAsync<ApplyForTravelException>(async () => await sut.ApplyUserForTravel(travelId, userId));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserAlreadyApplied()
        {
            var userOne = 1;

            var userTwo = 1;

            var travelId = 3;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await sut.ApplyUserForTravel(travelId, userOne);

                await Assert.ThrowsExceptionAsync<EntityAlreadyExists>(async () => await sut.ApplyUserForTravel(travelId, userTwo));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserNotFound()
        {
            var travelId = 1;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.ApplyUserForTravel(travelId, int.MaxValue));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserIsBlocked()
        {
            var travelId = 1;

            var userId = 4;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                var helper = new UserService(assertContext, modelMapper.Object);

                await helper.BlockUserAsync(userId);

                await Assert.ThrowsExceptionAsync<BlockedStateException>(async () => await sut.ApplyUserForTravel(travelId, userId));
            }
        }
    }
}
