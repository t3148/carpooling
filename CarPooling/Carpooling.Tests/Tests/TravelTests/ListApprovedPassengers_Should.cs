﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.TravelTests
{
    [TestClass]
    public class ListApprovedPassengers_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.Statuses.AddRange(Utils.GetStatuses());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.Preferences.AddRange(Utils.GetPreferences());
                arrangeContext.TravelPreferences.AddRange(Utils.GetTravelPreferences());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnUsers_When_ParamsAreValid()
        {
            var expected = new UserPresentDTO()
            {
                Username = "viksana",
                FirstName = "Victor",
                LastName = "Yordanov",
                Email = "victor@gmail.com",
                PhoneNumber = "0896825109",
                ProfilePicture = "c7f485d1-3f1c-4356-ac54-a7cdeab8b167_face7.jpg"
            };

            var travelId = 2;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                var users = await sut.ListApprovedPassengersForTravel(travelId);

                var actual = users.First();

                Assert.AreEqual(expected.Username, actual.Username);
                Assert.AreEqual(expected.FirstName, actual.FirstName);
                Assert.AreEqual(expected.LastName, actual.LastName);
                Assert.AreEqual(expected.Email, actual.Email);
                Assert.AreEqual(expected.PhoneNumber, actual.PhoneNumber);
                Assert.AreEqual(expected.ProfilePicture, actual.ProfilePicture);
                

            }
        }

        [TestMethod]
        public async Task ThrowException_When_TravelNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.ListApprovedPassengersForTravel(int.MaxValue));
            }
        }
    }
}
