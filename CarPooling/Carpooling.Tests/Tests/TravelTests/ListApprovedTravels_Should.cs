﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.TravelTests
{
    [TestClass]
    public class ListApprovedTravels_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.Statuses.AddRange(Utils.GetStatuses());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.Preferences.AddRange(Utils.GetPreferences());
                arrangeContext.TravelPreferences.AddRange(Utils.GetTravelPreferences());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectApprovedTravels()
        {
            var expected = new TravelPresentDTO
            {
                Id = 2,
                StartingPoint = "Vraca",
                EndingPoint = "Gabrovo",
                DepartureTime = new DateTime(2022, 1, 12, 15, 30, 0),
                FreeSeats = 3,
                CreatedByUserId = 3,
                CreatedByUser = "Kiril Stanoev",
                Status = "Available"
            };

            var userId = 11;
            var status = "Available";

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new TravelService(assertContext, modelMapper.Object);

                var travels = await sut.ListApprovedTravelsAsync(userId, status);

                var actual = travels.First();

                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.StartingPoint, actual.StartingPoint);
                Assert.AreEqual(expected.EndingPoint, actual.EndingPoint);
                Assert.AreEqual(expected.DepartureTime, actual.DepartureTime);
                Assert.AreEqual(expected.FreeSeats, actual.FreeSeats);
                Assert.AreEqual(expected.CreatedByUserId, actual.CreatedByUserId);
                Assert.AreEqual(expected.CreatedByUser, actual.CreatedByUser);
                Assert.AreEqual(expected.Status, actual.Status);
            }
        }
    }
}
