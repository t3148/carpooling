﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.UserTests
{
    [TestClass]
    public class GetTopPassengers_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.UserRoles.AddRange(Utils.GetUserRoles());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectUsers_When_ParamsAreValid()
        {
            var expected = new PassengerPresentDTO()
            {
                FullName = "Kiril Stanoev",
                Rating = 5,
                Username = "k.stanoev",
                ProfilePicture = "cef26811-1f8e-4b8c-8cad-54d0e1622a0c_kstanoev.jpg"
            };


            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                var actualUsers = await sut.GetAllTopPassengersAsync();

                var actuals = actualUsers.OrderByDescending(x => x.Rating).Take(2).ToList();
                var actual = actuals.First();

                Assert.AreEqual(expected.FullName, actual.FullName);
                Assert.AreEqual(expected.Rating, actual.Rating);
                Assert.AreEqual(expected.Username, actual.Username);
                Assert.AreEqual(expected.ProfilePicture, actual.ProfilePicture);
                
                
            }
        }
    }
}
