﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.UserTests
{
    [TestClass]
    public class UpdateUser_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.UserRoles.AddRange(Utils.GetUserRoles());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task UpdateCorrectly_When_ParamsAreValid()
        {
            var expected = new UserCreateDTO()
            {
                Username = "imence",
                FirstName = "Purvo",
                LastName = "Vtoro",
                Email = "unikalenmeil@abv.bg",
                PhoneNumber = "0192537581",
                ProfilePicture = "image"
            };

            var userId = 2;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                var actual = await sut.UpdateAsync(userId, expected);

                Assert.AreEqual(expected.Username, actual.Username);
                Assert.AreEqual(expected.FirstName, actual.FirstName);
                Assert.AreEqual(expected.LastName, actual.LastName);
                Assert.AreEqual(expected.Email, actual.Email);
                Assert.AreEqual(expected.PhoneNumber, actual.PhoneNumber);
                Assert.AreEqual(expected.ProfilePicture, actual.ProfilePicture);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            var user = new UserCreateDTO();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.UpdateAsync(int.MaxValue, user));
            }
        }
    }
}
