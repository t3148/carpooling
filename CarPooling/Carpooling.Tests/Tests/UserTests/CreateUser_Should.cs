﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.UserTests
{
    [TestClass]
    public class CreateUser_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.UserRoles.AddRange(Utils.GetUserRoles());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task CreateUser_When_ParamsAreValid()
        {
            var user = new UserCreateDTO()
            {
                Username = "imence",
                FirstName = "Purvo",
                LastName = "Vtoro",
                Password = "TestPass-123",
                Email = "unikalenmeil@abv.bg",
                PhoneNumber = "0192537581",
                ProfilePicture = "image"
            };

            var expected = new UserPresentDTO()
            {
                Username = "imence",
                FirstName = "Purvo",
                LastName = "Vtoro",
                Email = "unikalenmeil@abv.bg",
                PhoneNumber = "0192537581",
                ProfilePicture = "image"
            };

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                var actual = await sut.CreateAsync(user);

                Assert.AreEqual(expected.Username, actual.Username);
                Assert.AreEqual(expected.FirstName, actual.FirstName);
                Assert.AreEqual(expected.LastName, actual.LastName);
                Assert.AreEqual(expected.Email, actual.Email);
                Assert.AreEqual(expected.PhoneNumber, actual.PhoneNumber);
                Assert.AreEqual(expected.ProfilePicture, actual.ProfilePicture);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UsernameDuplicate()
        {
            var user = new UserCreateDTO()
            {
                Username = "ataskov",
                FirstName = "Purvo",
                LastName = "Vtoro",
                Password = "TestPass-123",
                Email = "unikalenmeil@abv.bg",
                PhoneNumber = "0192537581",
                ProfilePicture = "image"
            };

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityAlreadyExists>(async () => await sut.CreateAsync(user));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_EmailDuplicate()
        {
            var user = new UserCreateDTO()
            {
                Username = "imence",
                FirstName = "Purvo",
                LastName = "Vtoro",
                Password = "TestPass-123",
                Email = "alex.taskovv@gmail.com",
                PhoneNumber = "0192537581",
                ProfilePicture = "image"
            };

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityAlreadyExists>(async () => await sut.CreateAsync(user));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_PhoneNumberDuplicate()
        {
            var user = new UserCreateDTO()
            {
                Username = "imence",
                FirstName = "Purvo",
                LastName = "Vtoro",
                Password = "TestPass-123",
                Email = "unikalenmeil@abv.bg",
                PhoneNumber = "0898322922",
                ProfilePicture = "image"
            };

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityAlreadyExists>(async () => await sut.CreateAsync(user));
            }
        }
    }
}
