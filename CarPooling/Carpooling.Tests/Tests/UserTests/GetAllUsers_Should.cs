﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.UserTests
{
    [TestClass]
    public class GetAllUsers_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.UserRoles.AddRange(Utils.GetUserRoles());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnAllUsers()
        {
            var expectedUsers = new List<UserPresentDTO>()
            {
                new UserPresentDTO()
                {
                    Username = "ataskov",
                    FirstName = "Alexander",
                    LastName = "Taskov",
                    Email = "alex.taskovv@gmail.com",
                    PhoneNumber = "0898322922",
                    ProfilePicture = "7b01f2ba-454d-40d4-b6be-8c4e2cf883c7_sahezarabota.jpg"
                },

                new UserPresentDTO()
                {
                    Username = "bkehayov",
                    FirstName = "Bogdan",
                    LastName = "Kehayov",
                    Email = "bogdan.kehayov@gmail.com",
                    PhoneNumber = "0894747949",
                    ProfilePicture = "c504be68-ba0f-4a72-8314-b0b0c15df4cc_82647092_10213410429654656_7370158159039561728_n.jpg"
                },
            };

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var test = assertContext.Users.Take(2);

                var sut = new UserService(assertContext, modelMapper.Object);

                var sutUsers = await sut.GetAllAsync();

                var actualUsers = sutUsers.OrderBy(x => x.FirstName).Take(2).ToList();

                for (int i = 0; i < actualUsers.Count; i++)
                {
                    var expected = expectedUsers[i];
                    var actual = actualUsers[i];

                    Assert.AreEqual(expected.Username, actual.Username);
                    Assert.AreEqual(expected.FirstName, actual.FirstName);
                    Assert.AreEqual(expected.LastName, actual.LastName);
                    Assert.AreEqual(expected.Email, actual.Email);
                    Assert.AreEqual(expected.PhoneNumber, actual.PhoneNumber);
                    Assert.AreEqual(expected.ProfilePicture, actual.ProfilePicture);
                }
            }
        }  
    }
}
