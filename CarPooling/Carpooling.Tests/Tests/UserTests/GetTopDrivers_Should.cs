﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.UserTests
{
    [TestClass]
    public class GetTopDrivers_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.UserRoles.AddRange(Utils.GetUserRoles());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectUsers_When_ParamsAreValid()
        {
            var expected = new DriverPresentDTO()
            {
                FullName = "Victor Yordanov",
                Username = "viksana",
                Rating = 5,
                ProfilePicture = "c7f485d1-3f1c-4356-ac54-a7cdeab8b167_face7.jpg"
            };

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                var users = await sut.GetAllTopDriversAsync();

                var actual = users.First();

                Assert.AreEqual(expected.FullName, actual.FullName);
                Assert.AreEqual(expected.Rating, actual.Rating);
                Assert.AreEqual(expected.Username, actual.Username);
                Assert.AreEqual(expected.ProfilePicture, actual.ProfilePicture);
            }
        }
    }
}
