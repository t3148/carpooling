﻿using CarPooling.Data;
using CarPooling.Services.Common;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Tests.Tests.UserTests
{
    [TestClass]
    public class BlockUser_Should : BaseTest
    {
        private DbContextOptions<CarPoolingContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new CarPoolingContext(this.options))
            {
                arrangeContext.Comments.AddRange(Utils.GetComments());
                arrangeContext.Feedbacks.AddRange(Utils.GetFeedbacks());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.PendingPassengers.AddRange(Utils.GetPendingPassengers());
                arrangeContext.ApprovedPassengers.AddRange(Utils.GetApprovedPassengers());
                arrangeContext.Travels.AddRange(Utils.GetTravels());
                arrangeContext.UserRoles.AddRange(Utils.GetUserRoles());
                arrangeContext.Roles.AddRange(Utils.GetRoles());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task BlockUser_When_ParamsAreValid()
        {
            var userId = 1;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);
                await sut.BlockUserAsync(userId);
                var user = await assertContext.Users.FindAsync(userId);
                Assert.IsTrue(user.IsBlocked);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserNotFound()
        {
            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await sut.BlockUserAsync(int.MaxValue));
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UserAlreadyBlocked()
        {
            var userId = 1;

            var modelMapper = new Mock<ModelMapper>();

            using (var assertContext = new CarPoolingContext(this.options))
            {
                var sut = new UserService(assertContext, modelMapper.Object);

                await sut.BlockUserAsync(userId);

                await Assert.ThrowsExceptionAsync<BlockedStateException>(async () => await sut.BlockUserAsync(userId));
            }
        }
    }
}
