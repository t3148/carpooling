using CarPooling.Data;
using CarPooling.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace CarPooling.Tests
{
    [TestClass]
    public class Utils
    {
        public static DbContextOptions<CarPoolingContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<CarPoolingContext>()
                       .UseInMemoryDatabase(databaseName)
                       .Options;
        }

        public static IEnumerable<City> GetCities()
        {
            return ModelBuilderExtensions.Cities;
        }

        public static IEnumerable<Comment> GetComments()
        {
            return ModelBuilderExtensions.Comments;
        }

        public static IEnumerable<Feedback> GetFeedbacks()
        {
            return ModelBuilderExtensions.Feedbacks;
        }

        public static IEnumerable<Role> GetRoles()
        {
            return ModelBuilderExtensions.Roles;
        }

        public static IEnumerable<Status> GetStatuses()
        {
            return ModelBuilderExtensions.Statuses;
        }

        public static IEnumerable<Travel> GetTravels()
        {
            return ModelBuilderExtensions.Travels;
        }

        public static IEnumerable<TravelPreference> GetTravelPreferences()
        {
            return ModelBuilderExtensions.TravelPreferences;
        }

        public static IEnumerable<Preference> GetPreferences()
        {
            return ModelBuilderExtensions.Preferences;
        }

        public static IEnumerable<User> GetUsers()
        {
            return ModelBuilderExtensions.Users;
        }

        public static IEnumerable<UserRole> GetUserRoles()
        {
            return ModelBuilderExtensions.UserRoles;
        }

        public static IEnumerable<ApprovedPassenger> GetApprovedPassengers()
        {
            return ModelBuilderExtensions.ApprovedPassengers;
        }

        public static IEnumerable<PendingPassenger> GetPendingPassengers()
        {
            return ModelBuilderExtensions.PendingPassengers;
        }

    }
}
