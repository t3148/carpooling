﻿using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using CarPooling.Web.Helpers;
using CarPooling.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Controllers
{
    [CustomAuthorization(Role = "Admin")]
    public class AdminController : Controller
    {
        private readonly ITravelService travelService;
        private readonly IUserService userService;

        public AdminController(ITravelService travelService, IUserService userService)
        {
            this.travelService = travelService;
            this.userService = userService;
        }

        public async Task<IActionResult> Users(string sortOrder, string username, string email, string phoneNumber, int pagenumber = 1)
        {
            try
            {
                var users = await this.FilterUsers(sortOrder, username, email, phoneNumber, pagenumber);

                return View(users);
            }
            catch (EntityNotFoundException)
            {
                return View();
            }

            
        }

        public async Task<IActionResult> Travels(string sortOrder, int id, string startingCity, string endingCity,
                                                    string user, DateTime? departureTime, string seats, string status, int pagenumber = 1)
        {
            var travels = await this.travelService.GetAllAsync();

            var filteredTravels = this.FilterTravels(travels, sortOrder, id, startingCity, endingCity, user, departureTime, seats, status, pagenumber);

            return View(filteredTravels);
        }

        //GET-Block
        public async Task<IActionResult> BlockUser(int id)
        {
            var userToBlock = await this.userService.GetAsync(id);

            return this.View(userToBlock);
        }

        //POST-Block
        public async Task<IActionResult> BlockUserPost(int id)
        {
            await this.userService.BlockUserAsync(id);

            return this.RedirectToAction(nameof(this.Users));
        }

        //GET-Unblock
        public async Task<IActionResult> UnblockUser(int id)
        {
            var userToUnblock = await this.userService.GetAsync(id);

            return this.View(userToUnblock);
        }

        //POST-Unblock
        public async Task<IActionResult> UnblockUserPost(int id)
        {
            await this.userService.UnblockUserAsync(id);

            return this.RedirectToAction(nameof(this.Users));
        }

        public async Task<IActionResult> DeleteTravel(int travelId)
        {
            var travelToDelete = await this.travelService.GetAsync(travelId);

            return View(travelToDelete);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteTravelPost(int travelId)
        {
            await this.travelService.DeleteAsync(travelId);

            return this.RedirectToAction("Index", "Home");
        }

        private PaginatedList<TravelPresentDTO> FilterTravels(IEnumerable<TravelPresentDTO> travels,
                                                       string sortOrder, int id, string startingCity, string endingCity,
                                                       string user, DateTime? departureTime, string seats, string status, int pagenumber = 1)
        {
            this.SetTravelViewBags(id, sortOrder, startingCity, endingCity, user, departureTime, seats, pagenumber, status);

            var filteredTravels = this.travelService.FilterByMultipleCriteria(travels, startingCity, endingCity, user, departureTime, seats, default, status);

            filteredTravels = SortTravels(filteredTravels, sortOrder);

            return PaginatedList<TravelPresentDTO>.Create(filteredTravels, pagenumber, 5);
        }

        private static IEnumerable<TravelPresentDTO> SortTravels(IEnumerable<TravelPresentDTO> list, string sortOrder)
        {
            list = sortOrder switch
            {
                "name" => list.OrderBy(s => s.CreatedByUser),
                "name_desc" => list.OrderByDescending(s => s.CreatedByUser),
                "status" => list.OrderBy(s => s.Status),
                "status_desc" => list.OrderByDescending(s => s.Status),
                "startingCity_desc" => list.OrderByDescending(s => s.StartingPoint),
                "endingCity" => list.OrderBy(s => s.EndingPoint),
                "endingCity_desc" => list.OrderByDescending(s => s.EndingPoint),
                "departureTime" => list.OrderBy(s => s.DepartureTime),
                "departureTime_desc" => list.OrderByDescending(s => s.DepartureTime),
                "seats" => list.OrderBy(s => s.FreeSeats),
                "seats_desc" => list.OrderByDescending(s => s.FreeSeats),
                _ => list.OrderBy(s => s.StartingPoint),
            };
            return list;
        }

        private void SetTravelViewBags(int id, string sortOrder, string startingCity, string endingCity, string user, DateTime? departureTime, string seats, int page, string status)
        {
            ViewBag.SortOrder = sortOrder;
            ViewBag.StartSort = String.IsNullOrEmpty(sortOrder) ? "startingCity_desc" : "";
            ViewBag.EndSort = sortOrder == "endingCity" ? "endingCity_desc" : "endingCity";
            ViewBag.NameSort = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.DepartureSort = sortOrder == "departureTime" ? "departureTime_desc" : "departureTime";
            ViewBag.SeatSort = sortOrder == "seats" ? "seats_desc" : "seats";
            ViewBag.StatusSort = sortOrder == "status" ? "status_desc" : "status";
            ViewBag.TravelId = id;
            ViewBag.StartingCity = startingCity;
            ViewBag.EndingCity = endingCity;
            ViewBag.User = user;
            ViewBag.DepartureTime = departureTime;
            ViewBag.Seats = seats;
            ViewBag.Status = status;
        }

        private async Task<PaginatedList<UserPresentDTO>> FilterUsers(string sortOrder, string username, string email, string phoneNumber, int pagenumber = 1)
        {
            this.SetUserViewBags(sortOrder, username, email, phoneNumber);

            var users = await this.userService.GetAllAsync();

            if (!string.IsNullOrEmpty(username))
            {
                users = await this.userService.SearchUsernameAsync(username);
            }
            if (!string.IsNullOrEmpty(email))
            {
                users = await this.userService.SearchEmailAsync(email);
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                users = await this.userService.SearchPhoneNumberAsync(phoneNumber);
            }

            users = SortUsers(users, sortOrder);

            return PaginatedList<UserPresentDTO>.Create(users, pagenumber, 5);
        }

        private static IEnumerable<UserPresentDTO> SortUsers(IEnumerable<UserPresentDTO> list, string sortOrder)
        {
            list = sortOrder switch
            {
                "firstName_desc" => list.OrderByDescending(s => s.FirstName),
                "lastName" => list.OrderBy(s => s.LastName),
                "lastName_desc" => list.OrderByDescending(s => s.LastName),
                "username" => list.OrderBy(s => s.Username),
                "username_desc" => list.OrderByDescending(s => s.Username),
                "email" => list.OrderBy(s => s.Email),
                "email_desc" => list.OrderByDescending(s => s.Email),
                "phoneNumber" => list.OrderBy(s => s.PhoneNumber),
                "phoneNumber_desc" => list.OrderByDescending(s => s.PhoneNumber),
                _ => list.OrderBy(s => s.FirstName),
            };
            return list;
        }

        private void SetUserViewBags(string sortOrder, string username, string email, string phoneNumber)
        {
            ViewBag.SortOrder = sortOrder;
            ViewBag.FirstNameSort = string.IsNullOrEmpty(sortOrder) ? "firstName_desc" : "";
            ViewBag.LastNameSort = sortOrder == "lastName" ? "lastName_desc" : "lastName";
            ViewBag.UsernameSort = sortOrder == "username" ? "username_desc" : "username";
            ViewBag.EmailSort = sortOrder == "email" ? "email_desc" : "email";
            ViewBag.PhoneNumberSort = sortOrder == "phoneNumber" ? "phoneNumber_desc" : "phoneNumber";
            ViewBag.Username = username;
            ViewBag.Email = email;
            ViewBag.PhoneNumber = phoneNumber;
        }
    }
}
