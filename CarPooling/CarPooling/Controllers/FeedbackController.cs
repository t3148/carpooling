﻿using CarPooling.Data;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Services.Contracts;
using CarPooling.Web.Helpers;
using CarPooling.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Controllers
{
    [CustomAuthorization(Role = "User")]
    public class FeedbackController : Controller
    {
        private readonly IFeedbackService feedbackService;

        public FeedbackController(IFeedbackService feedbackService)
        {
            this.feedbackService = feedbackService;
        }

        public async Task<IActionResult> Index(int userId, string sortOrder, string name, string rating, string comment, int pagenumber = 1)
        {
            var feedbacks = await this.GetFeedbacks(userId, sortOrder, name, rating, comment, pagenumber);

            return this.View(feedbacks);
        }

        public async Task<IActionResult> ViewUserFeedback(int userId, string sortOrder, string name, string rating, string comment, int pagenumber = 1)
        {
            var feedbacks = await this.GetFeedbacks(userId, sortOrder, name, rating, comment, pagenumber);

            return this.View(feedbacks);
        }

        //GET-Create
        public IActionResult CreateFeedback(string toUserId, string travelId)
        {
            ViewBag.ToUserId = toUserId;
            ViewBag.TravelId = travelId;

            var feedback = new FeedbackCreateDTO();

            return this.View(feedback);
        }

        //Post-Create
        [HttpPost]
        public async Task<IActionResult> CreateFeedbackPost(FeedbackCreateDTO feedbackCreate, int travelId, string toUserId)
        {
            if (!this.ModelState.IsValid)
            {
                ViewBag.ToUserId = toUserId;
                ViewBag.TravelId = travelId;

                return this.View("CreateFeedback", feedbackCreate);
            }

            await this.feedbackService.CreateAsync(feedbackCreate, travelId);

            return this.RedirectToAction("Index", "Home");
        }

        private async Task<PaginatedList<FeedbackPresentDTO>> GetFeedbacks(int userId, string sortOrder, string name, string rating, string comment, int pagenumber = 1)
        {
            this.SetViewBags(userId, sortOrder, name, rating, comment);

            var feedbacks = await this.feedbackService.FilterByMultipleCriteria(userId, name, rating, comment);

            feedbacks = Sort(feedbacks, sortOrder);

            return PaginatedList<FeedbackPresentDTO>.Create(feedbacks, pagenumber, 10);
        }

        private void SetViewBags(int userId, string sortOrder, string name, string rating, string comment)
        {
            ViewBag.SortOrder = sortOrder;
            ViewBag.NameSortParm = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.RatingSortParm = string.IsNullOrEmpty(sortOrder) ? "rating_desc" : "";
            ViewBag.Name = name;
            ViewBag.Rating = rating;
            ViewBag.Comment = comment;
            ViewData["UserFeedbackNavigationId"] = userId;
        }

        private static IEnumerable<FeedbackPresentDTO> Sort(IEnumerable<FeedbackPresentDTO> list, string sortOrder)
        {
            list = sortOrder switch
            {
                "name" => list.OrderBy(s => s.FromUser),
                "name_desc" => list.OrderByDescending(s => s.FromUser),
                "rating_desc" => list.OrderByDescending(s => s.Rating),
                _ => list.OrderBy(s => s.Rating),
            };
            return list;
        }
    }
}
