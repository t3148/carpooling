﻿using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using CarPooling.Web.Helpers;
using CarPooling.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Controllers
{
    [CustomAuthorization(Role = "User")]
    public class MyTravelsController : Controller
    {
        private readonly ITravelService travelService;
        private readonly IUserService userService;

        public MyTravelsController(ITravelService travelService, IUserService userService)
        {
            this.travelService = travelService;
            this.userService = userService;
        }

        public async Task<IActionResult> OrganizedTravels(int id, string sortOrder, string startingCity, string endingCity,
                                                string user, DateTime? departureTime, string seats, string status, int pagenumber = 1)
        {
            var organizedTravels = await this.travelService.GetOrganizedTravelsAsync(id, "Available");

            var filteredTravels = this.Filter(organizedTravels, sortOrder, id, startingCity, endingCity, user, departureTime, seats, status, pagenumber);


            return View(filteredTravels);
        }

        public async Task<IActionResult> PendingPassengers(int id)
        {
            var pendingPassengers = await this.travelService.ListPendingPassengersForTravel(id);

            this.HttpContext.Session.SetString("CurrentTravelId", id.ToString());

            return View(pendingPassengers);
        }

        public async Task<IActionResult> ApprovedPassengers(int id)
        {
            var approvedPassengers = await this.travelService.ListApprovedPassengersForTravel(id);

            this.HttpContext.Session.SetString("CurrentTravelId", id.ToString());

            return View(approvedPassengers);
        }

        public async Task<IActionResult> Approve(int id)
        {
            var userToApprove = await this.userService.GetAsync(id);

            return View(userToApprove);
        }

        [HttpPost]
        public async Task<IActionResult> ApprovePost(int travelId, int passengerId)
        {
            await this.travelService.ApprovePassengerForTravel(passengerId, travelId);

            return this.RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Decline(int id)
        {
            var userToDecline = await this.userService.GetAsync(id);

            return View(userToDecline);
        }

        [HttpPost]
        public async Task<IActionResult> DeclinePost(int travelId, int passengerId)
        {
            await this.travelService.DeclinePassengerFromTravel(passengerId, travelId);

            return this.RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Reject(int id)
        {
            var userToReject = await this.userService.GetAsync(id);

            return View(userToReject);
        }

        [HttpPost]
        public async Task<IActionResult> RejectPost(int travelId, int passengerId, int driverId)
        {
            await this.travelService.RejectParticipationInTravel(passengerId, travelId, driverId);

            return this.RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> PendingTravels(int userId)
        {
            var pendingTravels = await this.travelService.ListPendingTravelsAsync(userId);

            return View(pendingTravels);
        }

        public async Task<IActionResult> ApprovedTravels(int userId)
        {
            var approvedTravels = await this.travelService.ListApprovedTravelsAsync(userId, "Available");

            return View(approvedTravels);
        }

        public async Task<IActionResult> Cancel(int travelId)
        {
            var travelToCancel = await this.travelService.GetAsync(travelId);

            return View(travelToCancel);
        }

        [HttpPost]
        public async Task<IActionResult> CancelPost(int travelId, int userId)
        {
            await this.travelService.CancelParticipationInTravel(userId, travelId);

            return this.RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> CancelTrip(int travelId)
        {
            var travelToCancel = await this.travelService.GetAsync(travelId);

            return View(travelToCancel);
        }

        [HttpPost]
        public async Task<IActionResult> CancelTripPost(int travelId, int userId)
        {
            try
            {
                await this.travelService.CancelTrip(userId, travelId);
            }
            catch (BlockedStateException x)
            {
                var travelToCancel = await this.travelService.GetAsync(travelId);
                this.ModelState.AddModelError("DepartureTime", x.Message);
                return this.View("CancelTrip", travelToCancel);
            }

            return this.RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> MarkAsComplete(int travelId)
        {
            var travelToComplete = await this.travelService.GetAsync(travelId);

            return View(travelToComplete);
        }

        [HttpPost]
        public async Task<IActionResult> MarkAsCompletePost(int travelId, int driverId)
        {
            try
            {
                await this.travelService.MarkTripAsComplete(driverId, travelId);
            }
            catch (BlockedStateException x)
            {
                var travelToComplete = await this.travelService.GetAsync(travelId);
                this.ModelState.AddModelError("DepartureTime", x.Message);
                return this.View("MarkAsComplete", travelToComplete);
            }

            return this.RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> PastOrganizedTravels(int id)
        {
            var pastOrganizedTravels = await this.travelService.GetOrganizedTravelsAsync(id, "Completed");

            return View(pastOrganizedTravels);
        }

        public async Task<IActionResult> PastApprovedTravels(int userId)
        {
            var pastApprovedTravels = await this.travelService.ListApprovedTravelsAsync(userId, "Completed");

            return View(pastApprovedTravels);
        }

        public async Task<IActionResult> PastApprovedPassengers(int id)
        {
            var pastApprovedPassengers = await this.travelService.ListApprovedPassengersForTravel(id);

            ViewBag.TravelId = id.ToString();

            this.HttpContext.Session.SetString("CurrentTravelId", id.ToString());

            return View(pastApprovedPassengers);
        }

        private PaginatedList<TravelPresentDTO> Filter(IEnumerable<TravelPresentDTO> travels,
                                                         string sortOrder, int id, string startingCity, string endingCity,
                                                         string user, DateTime? departureTime, string seats, string status, int pagenumber = 1)
        {
            this.SetViewBags(id, sortOrder, startingCity, endingCity, user, departureTime, seats, status);

            var filteredTravels = this.travelService.FilterByMultipleCriteria(travels, startingCity, endingCity, user, departureTime, seats, default, status);

            filteredTravels = Sort(filteredTravels, sortOrder);

            return PaginatedList<TravelPresentDTO>.Create(filteredTravels, pagenumber, 5);
        }

        private static IEnumerable<TravelPresentDTO> Sort(IEnumerable<TravelPresentDTO> list, string sortOrder)
        {
            list = sortOrder switch
            {
                "name" => list.OrderBy(s => s.CreatedByUser),
                "name_desc" => list.OrderByDescending(s => s.CreatedByUser),
                "status" => list.OrderBy(s => s.Status),
                "status_desc" => list.OrderByDescending(s => s.Status),
                "startingCity_desc" => list.OrderByDescending(s => s.StartingPoint),
                "endingCity" => list.OrderBy(s => s.EndingPoint),
                "endingCity_desc" => list.OrderByDescending(s => s.EndingPoint),
                "departureTime" => list.OrderBy(s => s.DepartureTime),
                "departureTime_desc" => list.OrderByDescending(s => s.DepartureTime),
                "seats" => list.OrderBy(s => s.FreeSeats),
                "seats_desc" => list.OrderByDescending(s => s.FreeSeats),
                _ => list.OrderBy(s => s.StartingPoint),
            };
            return list;
        }

        private void SetViewBags(int id, string sortOrder, string startingCity, string endingCity, string user, DateTime? departureTime, string seats, string status)
        {
            ViewBag.SortOrder = sortOrder;
            ViewBag.StartSort = String.IsNullOrEmpty(sortOrder) ? "startingCity_desc" : "";
            ViewBag.EndSort = sortOrder == "endingCity" ? "endingCity_desc" : "endingCity";
            ViewBag.NameSort = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.DepartureSort = sortOrder == "departureTime" ? "departureTime_desc" : "departureTime";
            ViewBag.SeatSort = sortOrder == "seats" ? "seats_desc" : "seats";
            ViewBag.StatusSort = sortOrder == "status" ? "status_desc" : "status";
            ViewBag.TravelId = id;
            ViewBag.StartingCity = startingCity;
            ViewBag.EndingCity = endingCity;
            ViewBag.User = user;
            ViewBag.DepartureTime = departureTime;
            ViewBag.Seats = seats;
            ViewBag.Status = status;

        }

    }
}
