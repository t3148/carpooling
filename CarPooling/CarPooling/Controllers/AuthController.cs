﻿using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using CarPooling.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly IAuthService authService;
        private readonly IUserService userService;
        private readonly WebModelMapper webModelMapper;

        public AuthController(IAuthService authService, IUserService userService, WebModelMapper webModelMapper)
        {
            this.authService = authService;
            this.userService = userService;
            this.webModelMapper = webModelMapper;
        }

        //GET /auth/login
        public IActionResult Login()
        {
            var loginViewModel = new LoginViewModel();

            return View(loginViewModel);
        }

        //POST: /auth/login
        [HttpPost]
        public async Task<IActionResult> Login([Bind("Username, Password")] LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }

            try
            {
                var user = await this.authService.TryGetUserAsync(loginViewModel.Username, loginViewModel.Password);
                this.HttpContext.Session.SetString("CurrentUser", user.Username);
                this.HttpContext.Session.SetString("CurrentUserId", user.Id.ToString());
                this.HttpContext.Session.SetString("UserFeedbackNavigationId", user.Id.ToString());
                this.HttpContext.Session.SetString("CurrentUserBlockedState", user.IsBlocked.ToString());
                this.HttpContext.Session.SetString("CurrentRoles", string.Join(',', user.Roles.Select(userRole => userRole.Role.Type)));
                this.HttpContext.Session.SetString("ProfilePicture", user.ProfilePicture);

                return this.RedirectToAction("index", "home");
            }
            catch (AuthenticationException e)
            {
                this.ModelState.AddModelError("Username", e.Message);
                return this.View(loginViewModel);
            }
        }

        //GET: /auth/logout
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");
            this.HttpContext.Session.Remove("CurrentRoles");

            return this.RedirectToAction("index", "home");
        }

        //GET /auth/register
        public IActionResult Register()
        {
            var registerViewModel = new RegisterViewModel();
            return View(registerViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Register([Bind("Username, Password, ConfirmPassword, FirstName, LastName, Email, PhoneNumber")] RegisterViewModel registerViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(registerViewModel);
            }

            try
            {
                this.userService.AlreadyExists(default, registerViewModel.Username, registerViewModel.Email, registerViewModel.PhoneNumber);
            }
            catch (EntityAlreadyExists x)
            {
                this.ModelState.AddModelError("Username", x.Message);
                return this.View(registerViewModel);
            }

            if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.View(registerViewModel);
            }

            var user = this.webModelMapper.ToModel(registerViewModel);
            await this.userService.CreateAsync(user);

            return this.RedirectToAction(nameof(this.Login));
        }
    }
}
