﻿using CarPooling.Data;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using CarPooling.Web.Helpers;
using CarPooling.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Controllers
{
    [CustomAuthorization(Role = "User")]
    public class TravelsController : Controller
    {
        private readonly ITravelService travelService;
        private readonly ICityService cityService;
        private readonly CarPoolingContext carPoolingContext;

        public TravelsController(ITravelService travelService, ICityService cityService, CarPoolingContext carPoolingContext)
        {
            this.travelService = travelService;
            this.cityService = cityService;
            this.carPoolingContext = carPoolingContext;
        }

        public async Task<IActionResult> Index(int id, string sortOrder, string startingCity, string endingCity, 
                                                string user,  DateTime? departureTime, string seats, string preferences, int pagenumber = 1)
        {
            if (!string.IsNullOrEmpty(preferences) && preferences.Contains("select preference", StringComparison.InvariantCultureIgnoreCase))
            {
                preferences = string.Empty;
            }

            ViewBag.Preferences = this.carPoolingContext.Preferences.Select(x => new SelectListItem()
            {
                Text = $"{x.Text}",
                Value = $"{x.Text}"
            });

            var availableTravels = await this.travelService.GetAvailableTravelsAsync(id, "Available");

            var filteredTravels = this.Filter(availableTravels, sortOrder, id, startingCity, endingCity, user, departureTime, seats, preferences, pagenumber);

            return View(filteredTravels);
        }

        private PaginatedList<TravelPresentDTO> Filter(IEnumerable<TravelPresentDTO> travels, 
                                                        string sortOrder, int id, string startingCity, string endingCity, 
                                                        string user, DateTime? departureTime, string seats, string preferences, int pagenumber = 1)
        {
            this.SetViewBags(id, sortOrder, startingCity, endingCity, user, departureTime, seats, preferences);

            var filteredTravels = this.travelService.FilterByMultipleCriteria(travels, startingCity, endingCity, user, departureTime, seats, preferences, default);

            filteredTravels = Sort(filteredTravels, sortOrder);
            
            return PaginatedList<TravelPresentDTO>.Create(filteredTravels, pagenumber, 5);
        }

        private static IEnumerable<TravelPresentDTO> Sort(IEnumerable<TravelPresentDTO> list, string sortOrder)
        {
            list = sortOrder switch
            {
                "name" => list.OrderBy(s => s.CreatedByUser),
                "name_desc" => list.OrderByDescending(s => s.CreatedByUser),
                "startingCity_desc" => list.OrderByDescending(s => s.StartingPoint),
                "endingCity" => list.OrderBy(s => s.EndingPoint),
                "endingCity_desc" => list.OrderByDescending(s => s.EndingPoint),
                "departureTime" => list.OrderBy(s => s.DepartureTime),
                "departureTime_desc" => list.OrderByDescending(s => s.DepartureTime),
                "seats" => list.OrderBy(s => s.FreeSeats),
                "seats_desc" => list.OrderByDescending(s => s.FreeSeats),
                _ => list.OrderBy(s => s.StartingPoint),
            };
            return list;
        }

        private void SetViewBags(int id, string sortOrder, string startingCity, string endingCity, string user, DateTime? departureTime, string seats, string preferences)
        {
            ViewBag.SortOrder = sortOrder;
            ViewBag.StartSort = String.IsNullOrEmpty(sortOrder) ? "startingCity_desc" : "";
            ViewBag.EndSort = sortOrder == "endingCity" ? "endingCity_desc" : "endingCity";
            ViewBag.NameSort = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.DepartureSort = sortOrder == "departureTime" ? "departureTime_desc" : "departureTime";
            ViewBag.SeatSort = sortOrder == "seats" ? "seats_desc" : "seats";
            ViewBag.TravelId = id;
            ViewBag.StartingCity = startingCity;
            ViewBag.EndingCity = endingCity;
            ViewBag.User = user;
            ViewBag.DepartureTime = departureTime;
            ViewBag.Seats = seats;
            ViewBag.Preference = preferences;
        }

        public async Task<IActionResult> Apply(int id)
        {
            var travel = await this.travelService.GetAsync(id);

            return View(travel);
        }

        [HttpPost]
        public async Task<IActionResult> ApplyPost(int travelId, int userId)
        {
            try
            {
                await this.travelService.ApplyUserForTravel(travelId, userId);

                return RedirectToAction("Index", "Home");
            }
            catch (EntityAlreadyExists x)
            {
                var travelToApplyTo = await this.travelService.GetAsync(travelId);
                this.ModelState.AddModelError("DepartureTime", x.Message);
                return this.View("Apply", travelToApplyTo);
            }

        }

        public async Task<IActionResult> CreateNewTravel()
        {
            ViewBag.CityDropDown = await this.cityService.GetCityDropDownAsync();
            ViewBag.Preferences = this.carPoolingContext.Preferences.Select(x => new SelectListItem
            {
                Text = x.Text,
                Value = x.Id.ToString()
            });

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewTravelPost(int userId, TravelCreateDTO travelCreateDTO)
        {
            if (!this.ModelState.IsValid)
            {
                ViewBag.CityDropDown = await this.cityService.GetCityDropDownAsync();
                ViewBag.Preferences = this.carPoolingContext.Preferences.Select(x => new SelectListItem
                {
                    Text = x.Text,
                    Value = x.Id.ToString()
                });

                return this.View("CreateNewTravel");
            }

            try
            {
                await this.travelService.CreateAsync(userId, travelCreateDTO);

                return RedirectToAction("Index", "Home");
            }
            catch (DateTimeInPastException x)
            {
                this.ModelState.AddModelError("DepartureTime", x.Message);
                return this.View("CreateNewTravel");
            }

        }
    }
}
