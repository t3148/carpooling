﻿using CarPooling.Data.Models;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using CarPooling.Web.Helpers;
using CarPooling.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Controllers
{
    [CustomAuthorization(Role = "User")]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IAuthService authService;
        private readonly IWebHostEnvironment webHostEnvironment;

        public UserController(IUserService userService, IAuthService authService, IWebHostEnvironment webHostEnvironment)
        {
            this.userService = userService;
            this.authService = authService;
            this.webHostEnvironment = webHostEnvironment;
        }

        public async Task<IActionResult> Index(int userId)
        {
            var user = await this.userService.GetAsync(userId);

            return View(user);
        }

        // GET-Update
        public async Task<IActionResult> Update(string username)
        {
            var user = await this.authService.TryGetUserAsync(username);

            var userView = new UserViewModel()
            {
                Username = user.Username,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
            };

            return this.View(userView);
        }

        // POST-Update
        [HttpPost]
        public async Task<IActionResult> Update(int id, UserViewModel userViewDTO)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(userViewDTO);
            }

            string uniqueFileName = UploadedFile(userViewDTO);

            UserCreateDTO user = new UserCreateDTO();

            user.Username = userViewDTO.Username;
            user.FirstName = userViewDTO.FirstName;
            user.LastName = userViewDTO.LastName;
            user.Email = userViewDTO.Email;
            user.PhoneNumber = userViewDTO.PhoneNumber;
            user.ProfilePicture = uniqueFileName;

            try
            {
                await this.userService.UpdateAsync(id, user);
            }
            catch (EntityAlreadyExists x)
            {
                this.ModelState.AddModelError("Email", x.Message);
                return this.View(userViewDTO);
            }

            if (!string.IsNullOrEmpty(user.ProfilePicture))
            {
                this.HttpContext.Session.SetString("ProfilePicture", user.ProfilePicture);
            }

            return this.RedirectToAction("Index", "Home");
        }

        // GET-ChangePassword
        public IActionResult ChangePassword()
        {
            return this.View(new ChangePasswordViewModel());
        }

        // POST-ChangePassword
        [HttpPost]
        public async Task<IActionResult> ChangePassword(int id, ChangePasswordViewModel changePasswordViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(changePasswordViewModel);
            }

            if (!changePasswordViewModel.Password.Equals(changePasswordViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.View(changePasswordViewModel);
            }

            await this.userService.ChangePassword(id, changePasswordViewModel.Password);

            return this.RedirectToAction("Index", "Home");
        }

        //GET-ViewUserProfile
        public async Task<IActionResult> ViewUserProfile(int userId)
        {
            var user = await this.userService.GetAsync(userId);

            return View(user);
        }

        private string UploadedFile(UserViewModel model)
        {
            string uniqueFileName = null;

            if (model.ProfilePicture != null)
            {
                string uploadsFolder = Path.Combine(webHostEnvironment.WebRootPath, "profile-pictures");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.ProfilePicture.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    model.ProfilePicture.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
    }
}
