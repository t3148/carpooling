﻿using CarPooling.Models;
using CarPooling.Services.Services.Contracts;
using CarPooling.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService userService;
        private readonly ITravelService travelService;
        private readonly ICityService cityService;

        public HomeController(IUserService userService, ITravelService travelService, ICityService cityService)
        {
            this.userService = userService;
            this.travelService = travelService;
            this.cityService = cityService;
        }

        public async Task<IActionResult> Index()
        {
            var users = await this.userService.GetAllAsync();
            var travels = await this.travelService.GetAllAsync();
            var cities = await this.cityService.GetAllAsync();

            var drivers = await this.userService.GetAllTopDriversAsync();
            var passengers = await this.userService.GetAllTopPassengersAsync();

            var multipleModel = new IndexMultipleModel
            {
                Users = users,
                Travels = travels.Where(x => x.Status.Contains("Completed")),
                Cities = cities,
                DriverRatings = drivers,
                PassengerRatings = passengers
            };

            return View(multipleModel);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult ErrorNotFound()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
