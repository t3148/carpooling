﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Helpers
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
    public class CustomAuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                var roles = context.HttpContext.Session.GetString("CurrentRoles").Split(',').ToList();

                if (!roles.Contains(this.Role))
                {
                    context.Result = new UnauthorizedResult();
                }
            }
            catch (NullReferenceException)
            {
                context.Result = new UnauthorizedResult();
            }
        }

        public string Role { get; set; }
    }
}
