﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Models
{
    public class RegisterViewModel
    {
        [Required, MinLength(2), MaxLength(20)]
        public string Username { get; set; }

        [Required]
        [RegularExpression(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=-_]).*$", ErrorMessage = "Password must contain: One upper case letter; One lower case letter; A number; A special symbol(!*@#$%^&+=-_).")]
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        [Required, MinLength(2), MaxLength(20)]
        public string FirstName { get; set; }

        [Required, MinLength(2), MaxLength(20)]
        public string LastName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Phone Number!")]
        public string PhoneNumber { get; set; }

        public string ProfilePicture { get; set; }
    }
}
