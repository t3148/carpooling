﻿using CarPooling.Services.DTOs.PresentDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Models
{
    public class IndexMultipleModel
    {
        public IEnumerable<UserPresentDTO> Users { get; set; }

        public IEnumerable<TravelPresentDTO> Travels { get; set; }

        public IEnumerable<CityPresentDTO> Cities { get; set; }

        public IEnumerable<FeedbackPresentDTO> Feedbacks { get; set; }

        public IEnumerable<PassengerPresentDTO> PassengerRatings { get; set; }

        public IEnumerable<DriverPresentDTO> DriverRatings { get; set; }
    }
}
