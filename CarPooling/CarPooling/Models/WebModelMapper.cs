﻿using CarPooling.Services.DTOs.CreateDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Models
{
    public class WebModelMapper
    {
        public UserCreateDTO ToModel(RegisterViewModel registerViewModel)
        {
            return new UserCreateDTO
            {
                Username = registerViewModel.Username,
                FirstName = registerViewModel.FirstName,
                LastName = registerViewModel.LastName,
                Password = registerViewModel.Password,
                Email = registerViewModel.Email,
                PhoneNumber = registerViewModel.PhoneNumber,
                ProfilePicture = "default-avatar.jpg"
            };
        }
    }
}
