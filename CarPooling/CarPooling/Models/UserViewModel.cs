﻿using CarPooling.Services.DTOs.CreateDTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Web.Models
{
    public class UserViewModel
    {
        [Required, MinLength(2), MaxLength(20)]
        public string Username { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} symbols!")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} symbols!")]
        public string LastName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Phone Number!")]
        public string PhoneNumber { get; set; }

        public IFormFile ProfilePicture { get; set; }
    }
}
