﻿using CarPooling.Data.Models;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.API.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly IAuthService authService;

        public AuthHelper(IAuthService authService)
        {
            this.authService = authService;
        }

        public async Task<User> TryGetAdminAsync(string username)
        {
            try
            {
                var user = await this.authService.TryGetUserAsync(username);

                var roles = user.Roles.Select(x => x.Role.Type);

                if (!roles.Contains("Admin"))
                {
                    throw new AuthenticationException("User is not an admin!");
                }

                return user;
            }
            catch (AuthenticationException x)
            {
                throw new AuthenticationException(x.Message);
            }
        }

    }
}
