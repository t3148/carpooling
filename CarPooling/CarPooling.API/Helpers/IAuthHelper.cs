﻿using CarPooling.Data.Models;
using System.Threading.Tasks;

namespace CarPooling.API.Helpers
{
    public interface IAuthHelper
    {
        Task<User> TryGetAdminAsync(string username);
    }
}
