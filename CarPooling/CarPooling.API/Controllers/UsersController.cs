﻿using CarPooling.API.Helpers;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace CarPooling.API.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IAuthHelper authHelper;

        public UsersController(IUserService userService, IAuthHelper authHelper)
        {
            this.userService = userService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets all the users from the database.")]
        public async Task<IActionResult> GetAllAsync([FromHeader] string usernameAuthorization, [FromQuery] string username, string email, string phoneNumber)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var users = await this.userService.GetAllAsync();

                if (!string.IsNullOrEmpty(username))
                {
                    users = await this.userService.SearchUsernameAsync(username);
                }
                if (!string.IsNullOrEmpty(email))
                {
                    users = await this.userService.SearchEmailAsync(email);
                }
                if (!string.IsNullOrEmpty(phoneNumber))
                {
                    users = await this.userService.SearchPhoneNumberAsync(phoneNumber);
                }

                return this.Ok(users);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }

        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Gets the specified by 'id' user from the database.")]
        public async Task<IActionResult> GetAsync([FromHeader] string usernameAuthorization, int id)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var user = await this.userService.GetAsync(id);

                return this.Ok(user);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }

        }

        [HttpPost("")]
        [SwaggerOperation(Summary = "Creates a new user.")]
        public async Task<IActionResult> CreateAsync([FromHeader] string usernameAuthorization, [FromBody] UserCreateDTO userDTO)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var newUser = await this.userService.CreateAsync(userDTO);

                return this.Created("post", newUser);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityAlreadyExists x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{id}")]
        [SwaggerOperation(Summary = "Updates the specified by 'id' existing user in the database.")]
        public async Task<IActionResult> UpdateAsync([FromHeader] string usernameAuthorization, int id, [FromBody] UserCreateDTO userCreateDTO)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var user = await this.userService.UpdateAsync(id, userCreateDTO);

                var editedUser = new UserPresentDTO(user);

                return this.Ok(editedUser);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
            catch (EntityAlreadyExists x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Deletes the specified by 'id' user from the database.")]
        public async Task<IActionResult> DeleteAsync([FromHeader] string usernameAuthorization, int id)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var user = await this.userService.DeleteAsync(id);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpPut("{id}/block")]
        [SwaggerOperation(Summary = "Blocks the specified by 'id' user in the database.")]
        public async Task<IActionResult> BlockUserAsync([FromHeader] string usernameAuthorization, int id)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.userService.BlockUserAsync(id);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
            catch (BlockedStateException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{id}/unblock")]
        [SwaggerOperation(Summary = "Unblocks the specified by 'id' user in the database.")]
        public async Task<IActionResult> UnblockUserAsync([FromHeader] string usernameAuthorization, int id)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.userService.UnblockUserAsync(id);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
            catch (BlockedStateException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpGet("feedback/topDrivers")]
        [SwaggerOperation(Summary = "Gets the top 10 drivers based on rating.")]
        public async Task<IActionResult> GetTopDrivers([FromHeader] string usernameAuthorization)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var users = await this.userService.GetAllTopDriversAsync();

                return this.Ok(users);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpGet("feedback/topPassengers")]
        [SwaggerOperation(Summary = "Gets the top 10 passengers based on rating.")]
        public async Task<IActionResult> GetTopPassengers([FromHeader] string usernameAuthorization)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var users = await this.userService.GetAllTopPassengersAsync();

                return this.Ok(users);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }
    }
}
