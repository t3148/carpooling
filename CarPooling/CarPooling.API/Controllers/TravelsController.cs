﻿using CarPooling.API.Helpers;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.API.Controllers
{
    [ApiController]
    [Route("api/travels")]
    public class TravelsController : ControllerBase
    {
        private readonly ITravelService travelService;
        private readonly IAuthHelper authHelper;

        public TravelsController(ITravelService travelService, IAuthHelper authHelper)
        {
            this.travelService = travelService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets all the travels from the database.")]
        public async Task<IActionResult> GetAllAsync([FromHeader] string usernameAuthorization, 
            [FromQuery] string startingCity, string endingCity, string createdByUser, DateTime? departureTime, string seats, string preferences, string status)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var travels = await this.travelService.GetAllAsync();

                travels = this.travelService.FilterByMultipleCriteria(travels, startingCity, endingCity, createdByUser, departureTime, seats, preferences, status);

                return this.Ok(travels);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Gets the specified by 'id' travel from the database.")]
        public async Task<IActionResult> GetAsync([FromHeader] string usernameAuthorization, int id)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var travel = await this.travelService.GetAsync(id);

                return this.Ok(travel);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpPost("")]
        [SwaggerOperation(Summary = "Creates a new travel.")]
        public async Task<IActionResult> CreateAsync([FromHeader] string usernameAuthorization, [FromHeader] int userId, [FromBody] TravelCreateDTO travelCreateDTO)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var newTravel = await this.travelService.CreateAsync(userId, travelCreateDTO);

                return this.Created("post", newTravel);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (BlockedStateException x)
            {
                return this.BadRequest(x.Message);
            }

        }

        [HttpPut("{id}")]
        [SwaggerOperation(Summary = "Updates the specified by 'id' existing travel in the database.")]
        public async Task<IActionResult> UpdateAsync([FromHeader] string usernameAuthorization, int id, [FromBody] TravelCreateDTO travelCreateDTO)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var updatedTravel = await this.travelService.UpdateAsync(id, travelCreateDTO);

                return this.Ok(updatedTravel);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Deletes the specified by 'id' travel from the database.")]
        public async Task<IActionResult> DeleteAsync([FromHeader] string usernameAuthorization, int id)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var travel = await this.travelService.DeleteAsync(id);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{travelId}/users/{userId}/apply")]
        [SwaggerOperation(Summary = "Apply user as passenger to the specified by 'id' travel.")]
        public async Task<IActionResult> ApplyUserForTravel([FromHeader] string usernameAuthorization, int travelId, int userId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.travelService.ApplyUserForTravel(travelId, userId);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (ApplyForTravelException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (EntityAlreadyExists x)
            {
                return this.BadRequest(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (BlockedStateException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpGet("{travelId}/pending-passengers")]
        [SwaggerOperation(Summary = "Gets the pending passengers for the specified by 'id' travel.")]
        public async Task<IActionResult> ListPendingPassengersForTravel([FromHeader] string usernameAuthorization, int travelId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var pendingPassengers = await this.travelService.ListPendingPassengersForTravel(travelId);

                return this.Ok(pendingPassengers);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (ApplyForTravelException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpGet("{travelId}/approved-passengers")]
        [SwaggerOperation(Summary = "Gets the approved passengers for the specified by 'id' travel.")]
        public async Task<IActionResult> ListApprovedPassengersForTravel([FromHeader] string usernameAuthorization, int travelId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var approvedPassengers = await this.travelService.ListApprovedPassengersForTravel(travelId);

                return this.Ok(approvedPassengers);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (ApplyForTravelException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{travelId}/users/{userId}/approve")]
        [SwaggerOperation(Summary = "Approve user as passenger to the specified by 'id' travel.")]
        public async Task<IActionResult> ApproveUserForTravel([FromHeader] string usernameAuthorization, int travelId, int userId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.travelService.ApprovePassengerForTravel(userId, travelId);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (ApplyForTravelException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (EntityAlreadyExists x)
            {
                return this.BadRequest(x.Message);
            }
            catch (EntityNotFoundException x)
            {

                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{travelId}/users/{userId}/reject")]
        [SwaggerOperation(Summary = "Reject user as passenger from the specified by 'id' travel.")]
        public async Task<IActionResult> DeclinePassengerFromTravel([FromHeader] string usernameAuthorization, int travelId, int userId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.travelService.DeclinePassengerFromTravel(userId, travelId);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (ApplyForTravelException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (EntityNotFoundException x)
            {

                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{travelId}/users/{userId}/cancel")]
        [SwaggerOperation(Summary = "Cancel the user's participation as passenger in the specified by 'id' travel.")]
        public async Task<IActionResult> CancelParticipationInTravel([FromHeader] string usernameAuthorization, int travelId, int userId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.travelService.CancelParticipationInTravel(userId, travelId);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (ApplyForTravelException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{travelId}/users/{passengerId}/users/{driverId}/reject")]
        [SwaggerOperation(Summary = "Reject the participation of passenger by driver in the specified by 'id' travel.")]
        public async Task<IActionResult> RejectParticipationInTravel([FromHeader] string usernameAuthorization, int travelId, int passengerId, int driverId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.travelService.RejectParticipationInTravel(passengerId, travelId, driverId);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (ApplyForTravelException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{travelId}/cancel-trip")]
        [SwaggerOperation(Summary = "Cancel the specified by 'id' travel.")]
        public async Task<IActionResult> CancelTrip([FromHeader] string usernameAuthorization, int travelId, [FromHeader] int driverId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.travelService.CancelTrip(driverId, travelId);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (BlockedStateException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{travelId}/mark-trip-complete")]
        [SwaggerOperation(Summary = "Mark the specified by 'id' travel as complete.")]
        public async Task<IActionResult> MarkTripAsComplete([FromHeader] string usernameAuthorization, int travelId, [FromHeader] int driverId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                await this.travelService.MarkTripAsComplete(driverId, travelId);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
            catch (BlockedStateException x)
            {
                return this.BadRequest(x.Message);
            }
        }
    }
}
