﻿using CarPooling.API.Helpers;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.API.Controllers
{
    [Route("api/cities")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        private readonly ICityService cityService;
        private readonly IAuthHelper authHelper;

        public CitiesController(ICityService cityService, IAuthHelper authHelper)
        {
            this.cityService = cityService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets all the cities from the database.")]
        public async Task<IActionResult> GetAllAsync([FromHeader] string usernameAuthorization)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var feedbacks = await this.cityService.GetAllAsync();

                return this.Ok(feedbacks);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }
    }
}
