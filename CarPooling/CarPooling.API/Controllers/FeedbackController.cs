﻿using CarPooling.API.Helpers;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.API.Controllers
{
    [Route("api/feedbacks")]
    [ApiController]
    public class FeedbacksController : ControllerBase
    {
        private readonly IFeedbackService feedBackService;
        private readonly IAuthHelper authHelper;

        public FeedbacksController(IFeedbackService feedBackService, IAuthHelper authHelper)
        {
            this.feedBackService = feedBackService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        [SwaggerOperation(Summary = "Gets all the feedbacks from the database.")]
        public async Task<IActionResult> GetAllFeedbackAsync([FromHeader] string usernameAuthorization)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var feedbacks = await this.feedBackService.GetAllFeedbackAsync();

                return this.Ok(feedbacks);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpGet("users/{userId}")]
        [SwaggerOperation(Summary = "Gets all the feedbacks a user has from the database.")]
        public async Task<IActionResult> GetAllUserFeedbackAsync([FromHeader] string usernameAuthorization, int userId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var feedbacks = await this.feedBackService.GetAllUserFeedbackAsync(userId);

                return this.Ok(feedbacks);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpGet("{feedbackId}")]
        [SwaggerOperation(Summary = "Get the specified by 'id' feedback from the database.")]
        public async Task<IActionResult> GetAsync([FromHeader] string usernameAuthorization, int feedbackId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var feedback = await this.feedBackService.GetAsync(feedbackId);

                return this.Ok(feedback);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpPost("travels/{travelId}")]
        [SwaggerOperation(Summary = "Create a new feedback.")]
        public async Task<IActionResult> CreateAsync([FromHeader] string usernameAuthorization, [FromBody]FeedbackCreateDTO feedbackCreateDTO, int travelId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var feedback = await this.feedBackService.CreateAsync(feedbackCreateDTO, travelId);

                return this.Ok(feedback);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
            catch (InvalidOperationException x)
            {
                return this.BadRequest(x.Message);
            }
        }

        [HttpPut("{feedbackId}")]
        [SwaggerOperation(Summary = "Updates the specified by 'id' existing feedback in the database.")]
        public async Task<IActionResult> UpdateAsync([FromHeader] string usernameAuthorization, int feedbackId, [FromBody] FeedbackCreateDTO feedbackCreateDTO)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var feedback = await this.feedBackService.UpdateAsync(feedbackId, feedbackCreateDTO);

                return this.Ok(feedback);
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.NotFound(x.Message);
            }
        }

        [HttpDelete("{feedbackId}")]
        [SwaggerOperation(Summary = "Deletes the specified by 'id' feedback from the database.")]
        public async Task<IActionResult> DeleteAsync([FromHeader] string usernameAuthorization, int feedbackId)
        {
            try
            {
                await this.authHelper.TryGetAdminAsync(usernameAuthorization);

                var feedback = await this.feedBackService.DeleteAsync(feedbackId);

                return this.Ok();
            }
            catch (AuthenticationException x)
            {
                return this.Unauthorized(x.Message);
            }
            catch (EntityNotFoundException x)
            {
                return this.BadRequest(x.Message);
            }
        }
    }
}
