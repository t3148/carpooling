﻿using CarPooling.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.PresentDTOs
{
    public class PassengerPresentDTO
    {
        public PassengerPresentDTO() { }

        public PassengerPresentDTO(User user)
        {
            this.FullName = $"{user.FirstName} {user.LastName}";
            this.Username = user.Username;
            this.Rating = Math.Round(user.ReceivedFeedbacks
                .Where(u => u.FromDriverToPassenger == true)
                .Select(y => y.Rating).Sum()
                / user.ReceivedFeedbacks
                .Where(u => u.FromDriverToPassenger == true)
                .Count(), 1);
            this.ProfilePicture = user.ProfilePicture;
        }

        public string FullName { get; set; }

        public string Username { get; set; }

        public double Rating { get; set; }

        public string ProfilePicture { get; set; }
    }
}
