﻿using CarPooling.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.PresentDTOs
{
    public class FeedbackPresentDTO
    {
        public FeedbackPresentDTO() { }

        public FeedbackPresentDTO(Feedback feedback)
        {
            Id = feedback.Id;
            Rating = feedback.Rating;
            FromUser = $"{feedback.FromUser.FirstName} {feedback.FromUser.LastName}";
            FromUserId = feedback.FromUserId;
            ToUser = $"{feedback.ToUser.FirstName} {feedback.ToUser.LastName}";
            if (feedback.Comment != null)
            {
                Comment = feedback.Comment.Text;
            }
            else
            {
                Comment = "No comment";
            }
        }

        [JsonIgnore]
        public int Id { get; set; }

        public double Rating { get; set; }

        public string FromUser { get; set; }

        [JsonIgnore]
        public int FromUserId { get; set; }

        public string ToUser { get; set; }

        public string Comment { get; set; }
    }
}
