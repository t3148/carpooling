﻿using CarPooling.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.PresentDTOs
{
    public class CityPresentDTO
    {
        public CityPresentDTO() { }

        public CityPresentDTO(City city)
        {
            this.Name = city.Name;
            this.Id = city.Id;
        }

        [JsonIgnore]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
