﻿using CarPooling.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.PresentDTOs
{
    public class TravelPresentDTO
    {
        public TravelPresentDTO() { }

        public TravelPresentDTO(Travel travel)
        {
            this.Id = travel.Id;
            this.StartingPoint = travel.StartingPoint.Name;
            this.EndingPoint = travel.EndingPoint.Name;
            this.DepartureTime = travel.DepartureTime;
            this.FreeSeats = travel.FreeSeats;
            this.CreatedByUser = $"{travel.CreatedByUser.FirstName} {travel.CreatedByUser.LastName}";
            this.CreatedByUserId = travel.CreatedByUserId;
            this.Status = travel.Status.Type;

            if (travel.Preferences.Any())
            {
                this.TravelPreferences = travel.Preferences.Select(p => p.Preference.Text);
            }
            else
            {
                this.TravelPreferences = new List<string>()
                {
                    "No preferences"
                };
            }
        }

        [JsonIgnore]
        public int Id { get; set; }

        public string StartingPoint { get; set; }

        public string EndingPoint { get; set; }

        public DateTime DepartureTime { get; set; }

        public int FreeSeats { get; set; }

        public string CreatedByUser { get; set; }

        [JsonIgnore]
        public int CreatedByUserId { get; set; }

        public string Status { get; set; }

        public IEnumerable<string> TravelPreferences { get; set; }
    }
}
