﻿using CarPooling.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.PresentDTOs
{
    public class UserPresentDTO
    {
        public UserPresentDTO() { }

        public UserPresentDTO(User user)
        {
            this.Id = user.Id;
            this.Username = user.Username;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Email = user.Email;
            this.PhoneNumber = user.PhoneNumber;
            this.ProfilePicture = user.ProfilePicture;
            this.IsBlocked = user.IsBlocked.ToString();
        }

        public UserPresentDTO(PendingPassenger pendingPassenger)
        {
            this.Id = pendingPassenger.User.Id;
            this.Username = pendingPassenger.User.Username;
            this.FirstName = pendingPassenger.User.FirstName;
            this.LastName = pendingPassenger.User.LastName;
            this.Email = pendingPassenger.User.Email;
            this.PhoneNumber = pendingPassenger.User.PhoneNumber;
            this.ProfilePicture = pendingPassenger.User.ProfilePicture;
            this.IsBlocked = pendingPassenger.User.IsBlocked.ToString();
        }

        public UserPresentDTO(ApprovedPassenger approvedPassenger)
        {
            this.Id = approvedPassenger.User.Id;
            this.Username = approvedPassenger.User.Username;
            this.FirstName = approvedPassenger.User.FirstName;
            this.LastName = approvedPassenger.User.LastName;
            this.Email = approvedPassenger.User.Email;
            this.PhoneNumber = approvedPassenger.User.PhoneNumber;
            this.ProfilePicture = approvedPassenger.User.ProfilePicture;
            this.IsBlocked = approvedPassenger.User.IsBlocked.ToString();
        }

        [JsonIgnore]
        public int Id { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string ProfilePicture { get; set; }

        public string IsBlocked { get; set; }
    }
}
