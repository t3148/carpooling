﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.CreateDTOs
{
    public class FeedbackCreateDTO
    {
        [Range(1, 5)]
        public double Rating { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int FromUserId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int ToUserId { get; set; }

        public string Comment { get; set; }
    }
}
