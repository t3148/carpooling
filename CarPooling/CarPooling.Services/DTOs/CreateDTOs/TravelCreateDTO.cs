﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.CreateDTOs
{
    public class TravelCreateDTO
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int StartingPointId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]

        public int EndingPointId { get; set; }

        [Required]
        public DateTime DepartureTime { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int FreeSeats { get; set; }

        public List<string> Preferences { get; set; }
    }
}
