﻿using CarPooling.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.CreateDTOs
{
    public class UserCreateDTO
    {
        [Required, MinLength(2), MaxLength(20)]
        public string Username { get; set; }

        [Required, MinLength(2), MaxLength(20)]
        public string FirstName { get; set; }

        [Required, MinLength(2), MaxLength(20)]
        public string LastName { get; set; }

        [RegularExpression(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=-]).*$", ErrorMessage = "Password must contain: One upper case letter; One lower case letter; A number; A special symbol(!*@#$%^&+=-).")]
        public string Password { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Phone Number!")]
        public string PhoneNumber { get; set; }

        public string ProfilePicture { get; set; }

    }
}
