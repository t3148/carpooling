﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.DTOs.CreateDTOs
{
    public class CommentCreateDTO
    {
        public string Text { get; set; }

        public int FeedbackId { get; set; }
    }
}
