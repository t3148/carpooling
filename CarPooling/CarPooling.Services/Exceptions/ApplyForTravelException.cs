﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Exceptions
{
    public class ApplyForTravelException : ApplicationException
    {
        public ApplyForTravelException()
        {
        }

        public ApplyForTravelException(string message)
            : base(message)
        {
        }
    }
}
