﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Exceptions
{
    public class EntityAlreadyExists : ApplicationException
    {
        public EntityAlreadyExists()
        {
        }

        public EntityAlreadyExists(string message)
            : base(message)
        {
        }
    }
}
