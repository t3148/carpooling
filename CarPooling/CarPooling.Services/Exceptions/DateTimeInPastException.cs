﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Exceptions
{
    public class DateTimeInPastException : ApplicationException
    {
        public DateTimeInPastException()
        {
        }

        public DateTimeInPastException(string message)
            : base(message)
        {
        }
    }
}
