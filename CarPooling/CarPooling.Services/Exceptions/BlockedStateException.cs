﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Exceptions
{
    public class BlockedStateException : ApplicationException
    {
        public BlockedStateException()
        {
        }

        public BlockedStateException(string message)
            : base(message)
        {
        }
    }
}
