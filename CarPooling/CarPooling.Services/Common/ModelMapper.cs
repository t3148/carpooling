﻿using CarPooling.Data.Models;
using CarPooling.Services.DTOs.CreateDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Common
{
    public class ModelMapper
    {
        public User ToModel(UserCreateDTO userCreateDTO)
        {
            return new User
            {
                Username = userCreateDTO.Username,
                FirstName = userCreateDTO.FirstName,
                LastName = userCreateDTO.LastName,
                Password = userCreateDTO.Password,
                Email = userCreateDTO.Email,
                PhoneNumber = userCreateDTO.PhoneNumber,
                ProfilePicture = userCreateDTO.ProfilePicture
            };
        }

        public Travel ToModel(TravelCreateDTO travelCreateDTO)
        {
            return new Travel
            {
                StartingPointId = travelCreateDTO.StartingPointId,
                EndingPointId = travelCreateDTO.EndingPointId,
                DepartureTime = travelCreateDTO.DepartureTime,
                FreeSeats = travelCreateDTO.FreeSeats,
                StatusId = 1,
                IsDeleted = false
            };
        }

        public Feedback ToModel(FeedbackCreateDTO feedbackCreateDTO)
        {
            return new Feedback
            {
                Rating = feedbackCreateDTO.Rating,
                FromUserId = feedbackCreateDTO.FromUserId,
                ToUserId = feedbackCreateDTO.ToUserId,
            };
        }

        public Comment ToModel(CommentCreateDTO commentCreateDTO)
        {
            return new Comment
            {
                Text = commentCreateDTO.Text,
                FeedbackId = commentCreateDTO.FeedbackId
            };
        }
    }
}
