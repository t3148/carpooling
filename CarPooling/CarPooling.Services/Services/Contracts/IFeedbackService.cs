﻿using CarPooling.Data.Models;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Services.Contracts
{
    public interface IFeedbackService
    {
        Task<IEnumerable<FeedbackPresentDTO>> GetAllFeedbackAsync();

        Task<IEnumerable<FeedbackPresentDTO>> GetAllUserFeedbackAsync(int userId);

        Task<FeedbackPresentDTO> GetAsync(int feedbackId);

        Task<FeedbackPresentDTO> CreateAsync(FeedbackCreateDTO feedbackCreateDTO, int travelId);

        Task<bool> DeleteAsync(int feedbackId);

        Task<FeedbackPresentDTO> UpdateAsync(int feedbackId, FeedbackCreateDTO feedbackCreateDTO);

        Task<IEnumerable<FeedbackPresentDTO>> FilterByMultipleCriteria(int id, string name, string rating, string comment);
    }
}
