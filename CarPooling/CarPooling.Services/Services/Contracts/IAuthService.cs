﻿using CarPooling.Data.Models;
using CarPooling.Services.DTOs.PresentDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Services.Contracts
{
    public interface IAuthService
    {
        Task<User> TryGetUserAsync(string username);

        Task<User> TryGetUserAsync(string username, string password);
    }
}
