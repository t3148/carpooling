﻿using CarPooling.Data.Models;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Services.Services.Contracts
{
    public interface IUserService
    {
        Task<IEnumerable<UserPresentDTO>> GetAllAsync();

        Task<UserPresentDTO> GetAsync(int id);

        Task<UserPresentDTO> CreateAsync(UserCreateDTO userCreateDTO);

        Task<User> UpdateAsync(int id, UserCreateDTO userCreateDTO);

        Task<bool> DeleteAsync(int id);

        Task<User> GetUserModelAsync(int id);

        Task ChangePassword(int id, string password);

        Task BlockUserAsync(int id);

        Task UnblockUserAsync(int id);

        Task<IEnumerable<UserPresentDTO>> SearchUsernameAsync(string username);

        Task<IEnumerable<UserPresentDTO>> SearchEmailAsync(string email);

        Task<IEnumerable<UserPresentDTO>> SearchPhoneNumberAsync(string phoneNumber);

        Task<IEnumerable<DriverPresentDTO>> GetAllTopDriversAsync();

        Task<IEnumerable<PassengerPresentDTO>> GetAllTopPassengersAsync();

        void AlreadyExists(int? id, string username, string email, string phoneNumber);
    }
}
