﻿using CarPooling.Data.Models;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Services.Contracts
{
    public interface ITravelService
    {
        Task<IEnumerable<TravelPresentDTO>> GetAllAsync();

        Task<IEnumerable<TravelPresentDTO>> GetOrganizedTravelsAsync(int id, string status);

        Task<IEnumerable<TravelPresentDTO>> GetAvailableTravelsAsync(int driverId, string status);

        Task<TravelPresentDTO> GetAsync(int id);

        Task<TravelPresentDTO> CreateAsync(int userId, TravelCreateDTO travelCreateDTO);

        Task<TravelPresentDTO> UpdateAsync(int id, TravelCreateDTO travelCreateDTO);

        Task<bool> DeleteAsync(int id);

        Task ApplyUserForTravel(int travelId, int userId);

        Task<IEnumerable<UserPresentDTO>> ListPendingPassengersForTravel(int id);

        Task<IEnumerable<UserPresentDTO>> ListApprovedPassengersForTravel(int id);

        Task<IEnumerable<TravelPresentDTO>> ListPendingTravelsAsync(int userId);

        Task<IEnumerable<TravelPresentDTO>> ListApprovedTravelsAsync(int userId, string status);

        Task ApprovePassengerForTravel(int userId, int travelId);

        Task DeclinePassengerFromTravel(int userId, int travelId);

        Task CancelParticipationInTravel(int userId, int travelId);

        Task RejectParticipationInTravel(int passengerId, int travelId, int driverId);

        Task CancelTrip(int driverId, int travelId);

        Task MarkTripAsComplete(int driverId, int travelId);

        IEnumerable<TravelPresentDTO> FilterByMultipleCriteria
            (IEnumerable<TravelPresentDTO> travels, string startingCity, string endingCity, string user, DateTime? departureTime, string seats, 
            string preferences, string status);
    }
}
