﻿using CarPooling.Services.DTOs.PresentDTOs;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Services.Contracts
{
    public interface ICityService
    {
        Task<IEnumerable<CityPresentDTO>> GetAllAsync();

        Task<IEnumerable<SelectListItem>> GetCityDropDownAsync();
    }
}
