﻿using CarPooling.Data;
using CarPooling.Data.Models;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPooling.Services.Services
{
    public class UserService : IUserService
    {
        private readonly CarPoolingContext dbContext;
        private readonly ModelMapper modelMapper;

        public UserService(CarPoolingContext database, ModelMapper modelMapper)
        {
            this.dbContext = database;
            this.modelMapper = modelMapper;
        }

        private IQueryable<User> UsersQuery
        {
            get
            {
                return this.dbContext.Users
                    .Include(x => x.ReceivedFeedbacks)
                    .Where(x => x.IsDeleted == false);
            }
        }

        public async Task<IEnumerable<UserPresentDTO>> GetAllAsync()
        {
            var userToPresent = await this.UsersQuery.Select(x => new UserPresentDTO(x)).ToListAsync();

            return userToPresent;
        }

        public async Task<User> GetUserModelAsync(int id)
        {
            var user = await this.UsersQuery.FirstOrDefaultAsync(x => x.Id == id);

            return user ?? throw new EntityNotFoundException("User with this 'id' was not found!");
        }

        public async Task<UserPresentDTO> GetAsync(int id)
        {
            var user = new UserPresentDTO(await this.GetUserModelAsync(id));

            return user;
        }

        public async Task<UserPresentDTO> CreateAsync(UserCreateDTO userCreateDTO)
        {
            this.AlreadyExists(default, userCreateDTO.Username, userCreateDTO.Email, userCreateDTO.PhoneNumber);

            var user = this.modelMapper.ToModel(userCreateDTO);

            await this.dbContext.Users.AddAsync(user);
            await this.dbContext.SaveChangesAsync();

            var newUser = await this.GetUserModelAsync(user.Id);

            var userRole = new UserRole()
            {
                UserId = newUser.Id,
                RoleId = 1
            };

            await this.dbContext.UserRoles.AddAsync(userRole);
            await this.dbContext.SaveChangesAsync();

            return await this.GetAsync(user.Id);
        }

        public async Task<User> UpdateAsync(int id, UserCreateDTO userCreateDTO)
        {
            var user = await this.GetUserModelAsync(id);

            this.AlreadyExists(id, userCreateDTO.Username, userCreateDTO.Email, userCreateDTO.PhoneNumber);

            user.Username = userCreateDTO.Username;
            user.FirstName = userCreateDTO.FirstName;
            user.LastName = userCreateDTO.LastName;
            await this.ChangePassword(id, userCreateDTO.Password);
            user.Email = userCreateDTO.Email;
            user.PhoneNumber = userCreateDTO.PhoneNumber;
            await this.SetProfilePicture(user, userCreateDTO.ProfilePicture);


            await this.dbContext.SaveChangesAsync();

            return user;
        }

        public async Task ChangePassword(int id, string password)
        {
            if (!string.IsNullOrEmpty(password))
            {
                var user = await this.GetUserModelAsync(id);

                user.Password = password;

                await this.dbContext.SaveChangesAsync();
            }
        }

        private async Task SetProfilePicture(User user, string picture)
        {
            if (!string.IsNullOrEmpty(picture))
            {
                user.ProfilePicture = picture;
                await this.dbContext.SaveChangesAsync();
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var user = await this.GetUserModelAsync(id);

            user.IsDeleted = true;
            await this.dbContext.SaveChangesAsync();

            return user.IsDeleted;
        }

        public async Task BlockUserAsync(int id)
        {
            var user = await this.GetUserModelAsync(id);

            if (user.IsBlocked)
            {
                throw new BlockedStateException("User already blocked.");
            }

            user.IsBlocked = true;

            await this.dbContext.SaveChangesAsync();
        }

        public async Task UnblockUserAsync(int id)
        {
            var user = await this.GetUserModelAsync(id);

            if (!user.IsBlocked)
            {
                throw new BlockedStateException("User isn't blocked.");
            }

            user.IsBlocked = false;

            await this.dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserPresentDTO>> SearchUsernameAsync(string username)
        {
            var users = await this.UsersQuery.Where(x => x.Username.Contains(username)).ToListAsync();

            var usersPresentDTO = users.Select(x => new UserPresentDTO(x));

            return usersPresentDTO;
        }

        public async Task<IEnumerable<UserPresentDTO>> SearchEmailAsync(string email)
        {
            var users = await this.UsersQuery.Where(x => x.Email.Contains(email)).ToListAsync();
            
            var usersPresentDTO = users.Select(x => new UserPresentDTO(x));

            return usersPresentDTO;
        }

        public async Task<IEnumerable<UserPresentDTO>> SearchPhoneNumberAsync(string phoneNumber)
        {
            var users = await this.UsersQuery.Where(x => x.PhoneNumber.Contains(phoneNumber)).ToListAsync();

            var usersPresentDTO = users.Select(x => new UserPresentDTO(x));

            return usersPresentDTO;
        }

        public async Task<IEnumerable<DriverPresentDTO>> GetAllTopDriversAsync()
        {
            var users = await this.UsersQuery.Where(x => x.ReceivedFeedbacks.Count != 0 && x.ReceivedFeedbacks
                                                .Where(y => y.FromDriverToPassenger == false)
                                                    .Count() != 0)
                                             .ToListAsync();

            var drivers = users.Select(x => new DriverPresentDTO(x))
                                                .OrderByDescending(x => x.Rating)
                                                .Take(10)
                                                .ToList();

            return drivers;
        }

        public async Task<IEnumerable<PassengerPresentDTO>> GetAllTopPassengersAsync()
        {
            var users = await this.UsersQuery.Where(x => x.ReceivedFeedbacks.Count != 0 && x.ReceivedFeedbacks
                                                .Where(y => y.FromDriverToPassenger == true)
                                                    .Count() != 0)
                                             .ToListAsync();

            var passengers = users.Select(x => new PassengerPresentDTO(x))
                                                .OrderByDescending(x => x.Rating)
                                                .Take(10)
                                                .ToList();

            return passengers;
        }

        private static void RegisterExists(IQueryable<User> users, string username, string email, string phoneNumber)
        {
            if (users.Where(x => x.Username == username).Count() != 0)
            {
                throw new EntityAlreadyExists("A user with this 'username' already exists!");
            }
            if (users.Where(x => x.Email == email).Count() != 0)
            {
                throw new EntityAlreadyExists("A user with this 'email' already exists!");
            }
            if (users.Where(x => x.PhoneNumber == phoneNumber).Count() != 0)
            {
                throw new EntityAlreadyExists("A user with this 'phone number' already exists!");
            }
        }

        public void AlreadyExists(int? id, string username, string email, string phoneNumber)
        {
            var users = this.UsersQuery;

            if (id.HasValue)
            {
                users = this.UsersQuery.Where(x => x.Id != id);
            }

            RegisterExists(users, username, email, phoneNumber);
        }

    }
}
