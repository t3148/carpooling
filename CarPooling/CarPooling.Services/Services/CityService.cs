﻿using CarPooling.Data;
using CarPooling.Data.Models;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Services
{
    public class CityService : ICityService
    {
        private readonly CarPoolingContext dbContext;

        public CityService(CarPoolingContext dbContext)
        {
            this.dbContext = dbContext;
        }

        private IQueryable<City> CitiesQuery
        {
            get
            {
                return this.dbContext.Cities;
            }
        }

        public async Task<IEnumerable<CityPresentDTO>> GetAllAsync()
        {
            var cities = await this.CitiesQuery
                .Select(c => new CityPresentDTO(c))
                .ToListAsync();

            return cities ?? throw new EntityNotFoundException("No destinations available at the moment.");
        }

        public async Task<IEnumerable<SelectListItem>> GetCityDropDownAsync()
        {
            var cities = await this.GetAllAsync();

            IEnumerable<SelectListItem> cityDropDown = cities.Select(c => new SelectListItem
            {
                Text = c.Name,
                Value = c.Id.ToString()
            });

            return cityDropDown;
        }
    }
}
