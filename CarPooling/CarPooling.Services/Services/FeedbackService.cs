﻿using CarPooling.Data;
using CarPooling.Data.Models;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly CarPoolingContext dbContext;
        private readonly ModelMapper modelMapper;

        public FeedbackService(CarPoolingContext database, ModelMapper modelMapper)
        {
            this.dbContext = database;
            this.modelMapper = modelMapper;
        }

        private IQueryable<Feedback> FeedbacksQuery
        {
            get
            {
                return this.dbContext.Feedbacks
                    .Include(fromUser => fromUser.FromUser)
                    .Include(toUser => toUser.ToUser)
                    .Include(c => c.Comment)
                    .Where(x => x.IsDeleted == false);
            }
        }

        private IQueryable<Travel> TravelsQuery
        {
            get
            {
                return this.dbContext.Travels
                    .Include(t => t.ApprovedPassengers)
                        .ThenInclude(t => t.User)
                    .Where(x => x.IsDeleted == false);
            }
        }

        public async Task<IEnumerable<FeedbackPresentDTO>> GetAllFeedbackAsync()
        {
            var feedbacksPresentDTO = await this.FeedbacksQuery
                                        .Select(x => new FeedbackPresentDTO(x))
                                        .ToListAsync();

            return feedbacksPresentDTO;
        }

        public async Task<IEnumerable<FeedbackPresentDTO>> GetAllUserFeedbackAsync(int userId)
        {
            var feedbacksPresentDTO = await this.FeedbacksQuery
                                        .Where(x => x.ToUserId == userId)
                                        .Select(y => new FeedbackPresentDTO(y))
                                        .ToListAsync();

            return feedbacksPresentDTO;
        }

        public async Task<FeedbackPresentDTO> GetAsync(int feedbackId)
        {
            var feedback = new FeedbackPresentDTO(await this.GetFeedbackModelAsync(feedbackId));

            return feedback;
        }
       
        public async Task<FeedbackPresentDTO> CreateAsync(FeedbackCreateDTO feedbackCreateDTO, int travelId)
        {
            var travel = await this.TravelsQuery
                            .FirstOrDefaultAsync(x => x.Id == travelId) 
                            ?? throw new EntityNotFoundException("Travel with this 'id' was not found!");

            if (travel.StatusId != 2)
            {
                throw new InvalidOperationException("You can't leave a feedback before the travel has been completed!");
            }

            var feedback = this.modelMapper.ToModel(feedbackCreateDTO);

            var passengerToCheck = travel.ApprovedPassengers.FirstOrDefault(x => x.TravelId == travel.Id && x.UserId == feedback.ToUserId);

            SetFeedbackType(feedback, travel, passengerToCheck);

            await this.dbContext.Feedbacks.AddAsync(feedback);
            await this.dbContext.SaveChangesAsync();

            await this.CreateCommentAsync(feedbackCreateDTO, feedback.Id);

            var feedbackPresent = await this.GetAsync(feedback.Id);

            return feedbackPresent;
        }

        public async Task<FeedbackPresentDTO> UpdateAsync(int feedbackId, FeedbackCreateDTO feedbackCreateDTO)
        {
            var feedback = await this.GetFeedbackModelAsync(feedbackId);

            feedback.Rating = feedbackCreateDTO.Rating;
            feedback.FromUserId = feedbackCreateDTO.FromUserId;
            feedback.ToUserId = feedbackCreateDTO.ToUserId;

            await this.UpdateCommentAsync(feedback, feedbackCreateDTO);

            await this.dbContext.SaveChangesAsync();
            
            var feedbackPresentDTO = await this.GetAsync(feedbackId);

            return feedbackPresentDTO;
        }
        
        public async Task<bool> DeleteAsync(int feedbackId)
        {
            var feedback = await this.GetFeedbackModelAsync(feedbackId);

            feedback.IsDeleted = true;
            await this.dbContext.SaveChangesAsync();

            return feedback.IsDeleted;
        }

        public async Task<IEnumerable<FeedbackPresentDTO>> FilterByMultipleCriteria(int id, string name, string rating, string comment)
        {
            var feedbacks = await this.GetAllUserFeedbackAsync(id);

            if (!string.IsNullOrEmpty(name))
            {
                feedbacks = feedbacks.Where(x => x.FromUser.Contains(name, StringComparison.OrdinalIgnoreCase)).ToList();
            }
            if (!string.IsNullOrEmpty(rating))
            {
                var check = double.TryParse(rating, out double compare);

                if (check)
                {
                    feedbacks = feedbacks.Where(x => x.Rating >= compare).ToList();
                }
            }
            if (!string.IsNullOrEmpty(comment))
            {
                feedbacks = feedbacks.Where(x => x.Comment.Contains(comment, StringComparison.OrdinalIgnoreCase)).ToList();
            }

            return feedbacks;
        }

        private static void SetFeedbackType(Feedback feedback, Travel travel, ApprovedPassenger approvedPassenger)
        {
            if (feedback.FromUserId == travel.CreatedByUserId && approvedPassenger != null)
            {
                feedback.FromDriverToPassenger = true;
            }
            else if (feedback.ToUserId != travel.CreatedByUserId && approvedPassenger != null)
            {
                feedback.FromDriverToPassenger = false;
            }
            else if (feedback.ToUserId == travel.CreatedByUserId && approvedPassenger == null)
            {
                feedback.FromDriverToPassenger = false;
            }
            else
            {
                throw new EntityNotFoundException("Drivers and passengers don't match!");
            }
        }

        private async Task<Feedback> GetFeedbackModelAsync(int feedbackId)
        {
            var feedback = await this.FeedbacksQuery.FirstOrDefaultAsync(x => x.Id == feedbackId);

            return feedback ?? throw new EntityNotFoundException("Feedback doesn't exist!");
        }

        private async Task CreateCommentAsync(FeedbackCreateDTO feedback, int feedbackId)
        {
            if (!string.IsNullOrEmpty(feedback.Comment))
            {
                var commentCreate = new CommentCreateDTO
                {
                    Text = feedback.Comment,
                    FeedbackId = feedbackId
                };

                var newComment = this.modelMapper.ToModel(commentCreate);

                await this.dbContext.Comments.AddAsync(newComment);
                await this.dbContext.SaveChangesAsync();
            }
        }

        private async Task UpdateCommentAsync(Feedback feedback, FeedbackCreateDTO feedbackCreateDTO)
        {
            if (!string.IsNullOrEmpty(feedbackCreateDTO.Comment))
            {
                if (feedback.Comment != null)
                {
                    feedback.Comment.Text = feedbackCreateDTO.Comment;
                }
                else
                {
                    await this.CreateCommentAsync(feedbackCreateDTO, feedback.Id);
                }
            }
        }
    }
}

