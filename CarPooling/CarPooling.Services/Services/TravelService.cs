﻿using CarPooling.Data;
using CarPooling.Data.Models;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.CreateDTOs;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Services
{
    public class TravelService : ITravelService
    {
        private readonly CarPoolingContext dbContext;
        private readonly ModelMapper modelMapper;

        public TravelService(CarPoolingContext database, ModelMapper modelMapper)
        {
            this.dbContext = database;
            this.modelMapper = modelMapper;
        }

        private IQueryable<Travel> TravelsQuery
        {
            get
            {
                return this.dbContext.Travels
                    .Include(t => t.CreatedByUser)
                    .Include(t => t.EndingPoint)
                    .Include(t => t.StartingPoint)
                    .Include(t => t.Status)
                    .Include(t => t.Preferences)
                        .ThenInclude(p => p.Preference)
                    .Where(x => x.IsDeleted == false);
            }
        }

        private IQueryable<Travel> ApprovedPassengersTravelsQuery
        {
            get
            {
                return this.dbContext.Travels
                    .Include(t => t.CreatedByUser)
                    .Include(t => t.Status)
                    .Include(t => t.ApprovedPassengers)
                        .ThenInclude(t => t.User)
                    .Where(x => x.IsDeleted == false);
            }
        }

        private IQueryable<Travel> PendingPassengersTravelsQuery
        {
            get
            {
                return this.dbContext.Travels
                    .Include(t => t.CreatedByUser)
                    .Include(t => t.Status)
                    .Include(t => t.PendingPassengers)
                        .ThenInclude(t => t.User)
                    .Where(x => x.IsDeleted == false);
            }
        }

        private IQueryable<User> UsersQuery
        {
            get
            {
                return this.dbContext.Users
                    .Where(x => x.IsDeleted == false);
            }
        }

        public async Task<IEnumerable<TravelPresentDTO>> GetAllAsync()
        {
            var travels = await this.TravelsQuery
                .Select(t => new TravelPresentDTO(t))
                .ToListAsync();

            return travels ?? throw new EntityNotFoundException("There are no available travels yet!");
        }

        public async Task<IEnumerable<TravelPresentDTO>> GetAvailableTravelsAsync(int driverId, string status)
        {
            var travels = await this.TravelsQuery
                .Where(d => d.CreatedByUserId != driverId && d.Status.Type.Equals(status) && d.FreeSeats > 0 && d.DepartureTime > DateTime.Now)
                .Select(t => new TravelPresentDTO(t))
                .ToListAsync();

            return travels ?? throw new EntityNotFoundException("There are no available travels yet!");
        }

        public async Task<TravelPresentDTO> GetAsync(int id)
        {
            var travel = await this.TravelsQuery
                .FirstOrDefaultAsync(t => t.Id == id);

            if (travel == null)
            {
                throw new EntityNotFoundException("Travel with this 'id' was not found!");
            }

            var travelPresentDTO = new TravelPresentDTO(travel);

            return travelPresentDTO;
        }

        public async Task<IEnumerable<TravelPresentDTO>> GetOrganizedTravelsAsync(int id, string status)
        {
            var organizedTravels = await this.TravelsQuery
                .Where(d => d.CreatedByUserId == id && d.Status.Type.Equals(status))
                .Select(t => new TravelPresentDTO(t))
                .ToListAsync();

            return organizedTravels ?? throw new EntityNotFoundException("There are no organized travels yet!");
        }

        public async Task<TravelPresentDTO> CreateAsync(int userId, TravelCreateDTO travelCreateDTO)
        {
            var user = await this.UsersQuery.FirstOrDefaultAsync(u => u.Id == userId)
                ?? throw new EntityNotFoundException("User with the specified 'id' was not found!");

            if (user.IsBlocked)
            {
                throw new BlockedStateException("Blocked users cannot create travels!");
            }

            if (travelCreateDTO.DepartureTime <= DateTime.Now)
            {
                throw new DateTimeInPastException("Departure time cannot be in the past.");
            }

            var travel = this.modelMapper.ToModel(travelCreateDTO);

            travel.CreatedByUserId = userId;

            await this.dbContext.Travels.AddAsync(travel);
            await this.dbContext.SaveChangesAsync();

            if (travelCreateDTO.Preferences != null)
            {
                var travelPreference = travelCreateDTO.Preferences
                    .Select(x => new TravelPreference
                    {
                        PreferenceId = int.Parse(x),
                        TravelId = travel.Id
                    });

                foreach (var item in travelPreference)
                {
                    await this.dbContext.TravelPreferences.AddAsync(item);
                }

                await this.dbContext.SaveChangesAsync();
            }

            var createdTravel = await this.GetAsync(travel.Id);

            return createdTravel;
        }

        public async Task<TravelPresentDTO> UpdateAsync(int id, TravelCreateDTO travelCreateDTO)
        {

            var travel = await this.TravelsQuery
                .FirstOrDefaultAsync(x => x.Id == id);

            if (travel == null)
            {
                throw new EntityNotFoundException("Travel with this 'id' was not found!");
            }

            travel.StartingPointId = travelCreateDTO.StartingPointId;
            travel.EndingPointId = travelCreateDTO.EndingPointId;
            travel.DepartureTime = travelCreateDTO.DepartureTime;
            travel.FreeSeats = travelCreateDTO.FreeSeats;

            await this.dbContext.SaveChangesAsync();

            var updatedTravel = await this.GetAsync(travel.Id);

            return updatedTravel;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var travel = await this.TravelsQuery
                .FirstOrDefaultAsync(x => x.Id == id);

            if (travel == null)
            {
                throw new EntityNotFoundException("Travel with this 'id' was not found!");
            }

            travel.IsDeleted = true;
            await this.dbContext.SaveChangesAsync();

            return travel.IsDeleted;
        }

        public async Task ApplyUserForTravel(int travelId, int userId)
        {
            var travel = await this.TravelsQuery
                .FirstOrDefaultAsync(t => t.Id == travelId)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            if (travel.Status.Type.Equals("Completed"))
            {
                throw new ApplyForTravelException("Application unsuccessful. The travel is completed!");
            }

            if (travel.FreeSeats == 0)
            {
                throw new ApplyForTravelException("No available seats for this travel!");
            }

            if (this.dbContext.PendingPassengers.FirstOrDefault(p => p.TravelId == travelId && p.UserId == userId) != null)
            {
                throw new EntityAlreadyExists("You are already a pending passenger for this travel!");
            }

            if (this.dbContext.ApprovedPassengers.FirstOrDefault(p => p.TravelId == travelId && p.UserId == userId) != null)
            {
                throw new EntityAlreadyExists("You are already an approved passenger for this travel!");
            }

            var user = await this.UsersQuery
                .FirstOrDefaultAsync(u => u.Id == userId)
                ?? throw new EntityNotFoundException("User with the specified 'id' was not found!");

            if (user.IsBlocked)
            {
                throw new BlockedStateException("Blocked users cannot create travels!");
            }

            var newPendingPassenger = new PendingPassenger();
            newPendingPassenger.UserId = user.Id;
            newPendingPassenger.TravelId = travel.Id;


            travel.PendingPassengers.Add(newPendingPassenger);

            await this.dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserPresentDTO>> ListPendingPassengersForTravel(int id)
        {
            var travel = await this.PendingPassengersTravelsQuery
                .FirstOrDefaultAsync(t => t.Id == id)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            if (travel.Status.Type.Equals("Completed"))
            {
                throw new ApplyForTravelException("No listing available. The travel is already completed!");
            }

            var pendingPassengers = travel.PendingPassengers
                .Select(p => new UserPresentDTO(p))
                .ToList();

            return pendingPassengers;
        }

        public async Task<IEnumerable<UserPresentDTO>> ListApprovedPassengersForTravel(int id)
        {
            var travel = await this.ApprovedPassengersTravelsQuery
                .FirstOrDefaultAsync(t => t.Id == id)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            var approvedPassengers = travel.ApprovedPassengers
                .Select(a => new UserPresentDTO(a))
                .ToList();

            return approvedPassengers;
        }

        public async Task<IEnumerable<TravelPresentDTO>> ListPendingTravelsAsync(int userId)
        {
            var pendingTravels = await this.dbContext.PendingPassengers
                .Where(p => p.UserId == userId && !p.Travel.IsDeleted)
                .Select(t => new TravelPresentDTO
                {
                    Id = t.Travel.Id,
                    StartingPoint = t.Travel.StartingPoint.Name,
                    EndingPoint = t.Travel.EndingPoint.Name,
                    DepartureTime = t.Travel.DepartureTime,
                    FreeSeats = t.Travel.FreeSeats,
                    CreatedByUserId = t.Travel.CreatedByUserId,
                    CreatedByUser = $"{t.Travel.CreatedByUser.FirstName} {t.Travel.CreatedByUser.LastName}",
                    Status = t.Travel.Status.Type
                }).ToListAsync();

            return pendingTravels;
        }

        public async Task<IEnumerable<TravelPresentDTO>> ListApprovedTravelsAsync(int userId, string status)
        {
            var approvedTravels = await this.dbContext.ApprovedPassengers
                .Where(p => p.UserId == userId && p.Travel.Status.Type.Equals(status) && !p.Travel.IsDeleted)
                .Select(t => new TravelPresentDTO
                {
                    Id = t.Travel.Id,
                    StartingPoint = t.Travel.StartingPoint.Name,
                    EndingPoint = t.Travel.EndingPoint.Name,
                    DepartureTime = t.Travel.DepartureTime,
                    FreeSeats = t.Travel.FreeSeats,
                    CreatedByUserId = t.Travel.CreatedByUserId,
                    CreatedByUser = $"{t.Travel.CreatedByUser.FirstName} {t.Travel.CreatedByUser.LastName}",
                    Status = t.Travel.Status.Type
                }).ToListAsync();

            return approvedTravels;
        }

        public async Task ApprovePassengerForTravel(int userId, int travelId)
        {
            var travel = await this.PendingPassengersTravelsQuery
                .FirstOrDefaultAsync(t => t.Id == travelId)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            if (travel.Status.Type.Equals("Completed"))
            {
                throw new ApplyForTravelException("Operation unsuccessful. The travel is completed!");
            }

            if (travel.FreeSeats == 0)
            {
                throw new ApplyForTravelException("The travel has already filled its free seats!");
            }

            var user = await this.UsersQuery
                .FirstOrDefaultAsync(u => u.Id == userId)
                ?? throw new EntityNotFoundException("User with the specified 'id' was not found!");

            var pendingPassengerToRemove = travel.PendingPassengers
                .FirstOrDefault(p => p.TravelId == travelId && p.UserId == userId);

            if (pendingPassengerToRemove == null)
            {
                throw new EntityNotFoundException("The user is not in the candidate passengers pool!");
            }

            var newApprovedPassenger = new ApprovedPassenger();
            newApprovedPassenger.UserId = user.Id;
            newApprovedPassenger.TravelId = travel.Id;

            travel.ApprovedPassengers.Add(newApprovedPassenger);

            travel.FreeSeats--;

            travel.PendingPassengers.Remove(pendingPassengerToRemove);

            await this.dbContext.SaveChangesAsync();
        }

        public async Task DeclinePassengerFromTravel(int userId, int travelId)
        {
            var travel = await this.PendingPassengersTravelsQuery
                .FirstOrDefaultAsync(t => t.Id == travelId)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            if (travel.Status.Type.Equals("Completed"))
            {
                throw new ApplyForTravelException("Operation unsuccessful. The travel is completed!");
            }

            var user = await this.UsersQuery
                .FirstOrDefaultAsync(u => u.Id == userId)
                ?? throw new EntityNotFoundException("User with the specified 'id' was not found!");

            var pendingPassengerToRemove = travel.PendingPassengers.FirstOrDefault(p => p.TravelId == travelId && p.UserId == userId);

            if (pendingPassengerToRemove == null)
            {
                throw new EntityNotFoundException("The user is not in the candidate passengers pool!");
            }

            travel.PendingPassengers.Remove(pendingPassengerToRemove);

            await this.dbContext.SaveChangesAsync();
        }

        public async Task CancelParticipationInTravel(int userId, int travelId)
        {
            var travel = await this.ApprovedPassengersTravelsQuery
                .FirstOrDefaultAsync(t => t.Id == travelId)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            if (travel.Status.Type.Equals("Completed"))
            {
                throw new ApplyForTravelException("Operation unsuccessful. The travel is completed!");
            }

            var user = await this.UsersQuery
                .FirstOrDefaultAsync(u => u.Id == userId)
                ?? throw new EntityNotFoundException("User with the specified 'id' was not found!");

            var approvedPassengerToRemove = travel.ApprovedPassengers
                .FirstOrDefault(p => p.TravelId == travelId && p.UserId == userId);

            if (approvedPassengerToRemove == null)
            {
                throw new EntityNotFoundException("The user is not in the approved passengers pool!");
            }

            travel.FreeSeats++;

            travel.ApprovedPassengers.Remove(approvedPassengerToRemove);

            await this.dbContext.SaveChangesAsync();
        }

        public async Task RejectParticipationInTravel(int passengerId, int travelId, int driverId)
        {
            var travel = await this.ApprovedPassengersTravelsQuery
                .FirstOrDefaultAsync(t => t.Id == travelId)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            if (travel.Status.Type.Equals("Completed"))
            {
                throw new ApplyForTravelException("Operation unsuccessful. The travel is completed!");
            }

            if (travel.CreatedByUserId != driverId)
            {
                throw new ApplyForTravelException("You are not the driver and cannot reject passengers from the travel!");
            }

            var user = await this.UsersQuery.FirstOrDefaultAsync(u => u.Id == passengerId)
                ?? throw new EntityNotFoundException("User with the specified 'id' was not found!");

            var approvedPassengerToRemove = travel.ApprovedPassengers
                .FirstOrDefault(p => p.TravelId == travelId && p.UserId == passengerId);

            if (approvedPassengerToRemove == null)
            {
                throw new EntityNotFoundException("The user is not in the approved passengers pool!");
            }

            travel.FreeSeats++;

            travel.ApprovedPassengers.Remove(approvedPassengerToRemove);

            await this.dbContext.SaveChangesAsync();
        }

        public async Task CancelTrip(int driverId, int travelId)
        {
            var travel = await this.TravelsQuery
                .FirstOrDefaultAsync(t => t.Id == travelId)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            var driver = await this.TravelsQuery.
                FirstOrDefaultAsync(u => u.CreatedByUserId == driverId)
                ?? throw new EntityNotFoundException("Driver with the specified 'id' was not found!");

            if (travel.DepartureTime < DateTime.Now)
            {
                throw new BlockedStateException("Cannot cancel a trip after the departure time!");
            }

            travel.IsDeleted = true;

            await this.dbContext.SaveChangesAsync();
        }

        public async Task MarkTripAsComplete(int driverId, int travelId)
        {
            var travel = await this.TravelsQuery
                .FirstOrDefaultAsync(t => t.Id == travelId)
                ?? throw new EntityNotFoundException("Travel with the specified 'id' was not found!");

            var driver = await this.TravelsQuery
                .FirstOrDefaultAsync(u => u.CreatedByUserId == driverId)
                ?? throw new EntityNotFoundException("Driver with the specified 'id' was not found!");

            if (travel.DepartureTime > DateTime.Now)
            {
                throw new BlockedStateException("Cannot mark the trip as complete before the departure time!");
            }

            if (travel.StatusId == 2)
            {
                throw new BlockedStateException("Travel is already marked as complete!");
            }

            travel.StatusId = 2;

            await this.dbContext.SaveChangesAsync();
        }

        public IEnumerable<TravelPresentDTO> FilterByMultipleCriteria
            (IEnumerable<TravelPresentDTO> travels, string startingCity, string endingCity, string user, DateTime? departureTime, string seats, 
                        string preferences, string status)
        {
            if (!string.IsNullOrEmpty(startingCity))
            {
                travels = travels.Where(x => x.StartingPoint.Contains(startingCity, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            if (!string.IsNullOrEmpty(endingCity))
            {
                travels = travels.Where(x => x.EndingPoint.Contains(endingCity, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            if (!string.IsNullOrEmpty(user))
            {
                travels = travels.Where(x => x.CreatedByUser.Contains(user, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            if (departureTime.HasValue)
            {
                travels = travels.Where(x => x.DepartureTime >= departureTime).ToList();
            }
            if (!string.IsNullOrEmpty(seats))
            {
                var check = int.TryParse(seats, out int compare);

                if (check)
                {
                    travels = travels.Where(x => x.FreeSeats >= compare).ToList();
                }
            }
            if (!string.IsNullOrEmpty(status))
            {
                travels = travels.Where(x => x.Status.Contains(status, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            if (!string.IsNullOrEmpty(preferences))
            {
                travels = travels.Where(x => x.TravelPreferences.Where(y => y.Contains(preferences, StringComparison.InvariantCultureIgnoreCase)).Any()).ToList();
            }

            return travels;
        }
    }
}
