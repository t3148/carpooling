﻿using CarPooling.Data;
using CarPooling.Data.Models;
using CarPooling.Services.Common;
using CarPooling.Services.DTOs.PresentDTOs;
using CarPooling.Services.Exceptions;
using CarPooling.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling.Services.Services
{
    public class AuthService : IAuthService
    {
        private readonly CarPoolingContext carPoolingContext;

        public AuthService(CarPoolingContext carPoolingContext)
        {
            this.carPoolingContext = carPoolingContext;
        }

        private IQueryable<User> UsersQuery
        {
            get
            {
                return this.carPoolingContext.Users
                    .Include(x => x.Roles)
                        .ThenInclude(x => x.Role)
                    .Where(x => x.IsDeleted == false);
            }
        }

        public async Task<User> TryGetUserAsync(string username)
        {
            try
            {
                var user = await this.UsersQuery
                    .FirstOrDefaultAsync(x => x.Username == username)
               ?? throw new EntityNotFoundException("User with this 'username' doesn't exist!");

                return user;
            }
            catch (EntityNotFoundException x)
            {
                throw new AuthenticationException(x.Message);
            }
        }

        public async Task<User> TryGetUserAsync(string username, string password)
        {
            try
            {
                var user = await this.TryGetUserAsync(username);

                if (user.Password != password)
                {
                    throw new AuthenticationException("Invalid password.");
                }

                return user;
            }
            catch (AuthenticationException x)
            {
                throw new AuthenticationException(x.Message);
            }
        }
    }
}
