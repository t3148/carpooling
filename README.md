
![Logo](Utilities/readme-logo-150px.png)


# CarPooling by Alexander Taskov & Bogdan Kehayov


Carpooling is a web application that enables you to share your travel from one location to another with other passengers. Every user can either organize a shared travel or request to join someone else’s travel.

### Public Part

The public part is accessible without authentication i.e., for anonymous users. 
Anonymous users have the ability to register.

Anonymous users can see detailed information about Carpooling and its features as well as how many people are using the platform, how many travels have happened and how many destinations are available.

Anonymous users can see list of the top 10 travel organizers and top 10 passengers.

### Private part

Accessible only if the user is authenticated.

Users have the ability to login/logout, update their profile and set a profile photo.

Each user can create a new travel that he is planning and browse the available trips created by other users with an option to sort and filter them. The list with trips supports pagination.

Each user has the ability to apply for a trip as a passenger. The driver can approve/decline passengers from the candidates’ pool. After approval from the driver, passengers can still cancel their participation and driver can still reject a passenger.

The driver can cancel a trip before the departure time and mark the trip as complete after the departure time.

When the trip is complete the passengers can leave feedback about the driver as well as the driver can leave feedback for every passenger. The feedback includes numeric rating (from 1 to 5) and optional text comment.

Each user has the ability to view all his travels, all his feedback and all feedback for any other user.

### Administrative part

Accessible to users with administrative privileges.

Admin users can see list of all users and block/unblock them. A blocked user cannot create travels and apply for travels.

Admin users have the ability to view a list of all travels with option to delete them.


## Documentation

[Swagger on Localhost](https://localhost:5003/swagger/index.html)


## Installation

1. Clone the repository locally;

2. Open the source code using CarPooling.sln through Visual Studio;

3. For API: Open StartUp.cs in CarPooling project and configure your local SQL Server connection string;

4. For MVC: Open StartUp.cs in CarPooling.API project and configure your local SQL Server connection string;

5. Go to Tools -> NuGet Package Manager -> Package Manager Console;

6. Add Initial migration(if not available already) with the following command:
```bash
  Add-Migration Initial
```

7. Update the database with the following command:
```bash
  Update-Database
```

8. Run the project locally (CTRL + F5) and enjoy using it on [the following address](https://localhost:5001/). :)    
## Database relations

![Database-relations](Utilities/database-diagram.jpg)